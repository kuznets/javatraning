package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/utkonos")
public class UtkonosServlet extends HttpServlet {

    private String readURL2(String index) {

        StringBuilder urldata = new StringBuilder();
        String newLine = System.lineSeparator();
        try {
//            URL url = new URL("http://www.utkonos.ru/cat/" + index);
            URL url = new URL("https://www.e-maxima.ee/Pages/Search/Products.aspx?rexona&SortBy=Discounts&SortAsc=False&ProductName=&PriceFrom=&PriceTo=&PackageType=&CountryOfOrigin=&FilterSubmited=False&utm_source=internal&utm_medium=banner&utm_campaign=rexona_08042016");
            InputStream inputStream = url.openStream();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line = "";
                while (line != null) {
                    line = bufferedReader.readLine();
                    urldata.append(line).append(newLine);
                }
            } catch (IOException ex) {
                Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return parseData2(urldata.toString());
    }

    private String parseData2(String src) {
        String o = "Ошибка...", item;
        src = src.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "");
        String s[] = src.split("height=\"108\" width=\"108\"");
        if (s.length > 1) {
            o = "";
            for (int i = 1; i < s.length; i++) {
                item = s[i];
                int y = item.indexOf("</a> </h3");
                if (y > -1) {
                    int x = item.lastIndexOf(">", y);
                    if (x > -1) {
                        o += "<br><div>"
                                + "<input type=\"checkbox\" onclick=\"buttonClicked(this.parentElement, this)\" />"
                                + "<b>" + item.substring(x + 1, y) + "</b>";
                        String p = "class=\"newPrice\">";
                        int z = item.indexOf(p, y);
                        if (z > -1) {
                            o += "<br>" + item.substring(z + p.length(), item.indexOf("</span>", z));
                        } else {
                            p = "<p class=\"guide\">";
                            z = item.indexOf(p, y);
                            if (z > -1) {
                                o += "<br>" + item.substring(item.indexOf("<strong>", z) + 8, item.indexOf("</strong>", z));
                            }
                        }
                        o += "</div>\n\n";
                    }

                }
            }
        }

        return o;
    }

    private String readURL(String index) {

        String urlAddress = "https://www.e-maxima.ee/Products/sook-ja-jook/Puu-ja-koogiviljad" + "/" + index + ".aspx";
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(urlAddress);

            InputStream is = url.openStream(); //открываем входной поток
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is))) { //читаем текст в буфер из потока / декодируем байты в символы в заданной кодировке
                String line = "";
                String newLine = System.lineSeparator();//Перевод каретки для нашей ОС
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                    sb.append(newLine);
                }
            } catch (IOException ex) {
                Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UtkonosServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return parseData(sb.toString());
    }

    private String parseData(String src) {//получаем страницу

        String o = "Ошибка...", item;
        src = src.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "")
                .replaceAll(" +", " ");
        int x = src.indexOf("<table class=\"data type1\">");
        int y = src.indexOf("<span id=\"ctl00_MainContent_pager\">");
        o = "";

        /*
        <br>
        <div>
            <input type="checkbox" onclick="buttonClicked(this.parentElement, this)" />
            <div>
                <b>Арбуз 500г</b><br>
                <p>Цена: 10 еур/кг</p>
            </div>
        </div>
        
         */
        String split = "<td class=\"img noborder\">";
        String s[] = src.substring(x, y).split(split);
        String to = "</a> <a id=\"ctl00_MainContent_lstProduct_repProducts_";

//            for (String item : s) { not here as from i=1
        for (int i = 1; i < s.length; i++) {
            item = s[i];

            int u3 = item.indexOf(to);//ищем значение до куда искать 
            if (u3 > -1) {
                int u30 = item.lastIndexOf(">", u3);
                if (u30 > -1) {
                    o += "<br><div>"
                            + "<input type=\"checkbox\" onclick=\"buttonClicked(this.parentElement, this)\" />"
                            + "<div><b>" + item.substring(u30 + 1, u3) + "</b><br>";//Название продукта (Арбуз)
                }
            }

            String tmp = "", q = "<p class=\"guide\"> <strong>", q2 = "</strong>", q3 = "</div>";
            int z = 0, from = 0, z1, z2;
            /*
            String testString = "0123456789";
            int ts = testString.indexOf("78", from); //7
            int ts2 = 0;
            String ts1 = testString.substring(ts2, ts); //0123456
            z2 = 0;
            */
            z = item.indexOf(q, from); //поиск первого вхождения указанной подстроки с указанной позиции
            z1 = item.indexOf(q2, z);
            if (z1 > -1 && z1 > z) {
                tmp += "<p>Price: " + item.substring(z + q.length(), z1 - 3) + "eur/kg.</p>";
            }

            if (!tmp.equals("")) {
                o += tmp;
            }

            o += "</div></div>\n\n";
        }
        return o;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String index = request.getParameter("list");
        ///For *.jsp page
        request.setAttribute("listindex", index);
        request.setAttribute("output", readURL(index));
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        //For AJAX page
//        response.setContentType("text/plain;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println(readURL(index));
//        }
    }

}
