package servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SaveSelectionServlet")
public class SaveSelectionServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String data = request.getParameter("data");
        data = data.replaceFirst("<input type=\"checkbox\"[^>]*>", "");
        boolean checked = Boolean.parseBoolean(request.getParameter("checked"));

        String filepath = getServletContext().getRealPath("/") + "utkonos.txt";
        String fileData = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);

        String s[] = fileData.split("<br>\n<br>\n");
        List<String> list = new LinkedList<>(Arrays.asList(s));

        String str = "";

        if (checked) {
            if (!list.contains(data)) {
                str = data + "<br>\n<br>\n";
            }
        } else if (list.contains(data)) {
            list.remove(data);
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                str += it.next() + "<br>\n<br>\n";
            }
        }

        save2file(str, filepath, checked);
    }

    private synchronized void save2file(String source, String path, boolean append) {

        File f = new File(path);
        try {
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f, append), "UTF-8"));

            pw.write(source);
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

}
