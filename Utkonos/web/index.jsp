<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.2.0/css/foundation.css" rel="stylesheet">

    </head>
    <script>
        function buttonClicked(d, b) {
            var checked;
            if (b.checked) {
                checked = true;
            } else {
                checked = false;
            }
            saveSelection(d, '<div>' + d.innerHTML + '</div>', checked);
        }

        function saveSelection(d, s, checked) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function ()
            {
                if (xhr.readyState === 4 && xhr.status === 200)
                {
                    if (checked) {
                        d.style.backgroundColor = 'yellow';
                    } else {
                        d.style.backgroundColor = 'transparent';
                    }
                }
            };

            xhr.open("POST", 'SaveSelectionServlet', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send('data=' + s + '&checked=' + checked);
        }

    </script>
    <body>
        <a href="shopping.jsp" target="_blank">
            <button class="large radius">Открыть составленный список</button></a>
        <br>
        <%
            String listindex = "";
            if (request.getAttribute("listindex") != null) {
                listindex = request.getAttribute("listindex").toString();
            }
        %>
        <div style=" text-align: left; width: 55%; margin-left: auto; margin-right: auto;">
            <form action="utkonos">
                <select name="list" onchange="document.forms[0].submit()">
                    <option>Выбор раздела:</option>
                    <option <%
                        if (listindex.equals("Puuviljad")) {
                            out.println("selected");
                        }
                        %> value="Puuviljad">Фрукты</option>
                    <option <%
                        if (listindex.equals("Marjad")) {
                            out.println("selected");
                        }
                        %> value="Marjad">Ягоды</option>
                    <option <%
                        if (listindex.equals("Joogid")) {
                            out.println("selected");
                        }
                        %> value="Joogid">Напитки</option>
                    <option <%
                        if (listindex.equals("Varske-liha")) {
                            out.println("selected");
                        }
                        %> value="Varske-liha">Свежее мясо</option>
                    <option <%
                        if (listindex.equals("Lihatooted")) {
                            out.println("selected");
                        }
                        %> value="Lihatooted">Мясные продукты</option>
                    <option <%
                        if (listindex.equals("Varske-kala-ja-kalatooted")) {
                            out.println("selected");
                        }
                        %> value="Varske-kala-ja-kalatooted">Рыба и морепродукты</option>
                    <option <%
                        if (listindex.equals("Pagari-ja-kondiitritooted")) {
                            out.println("selected");
                        }
                        %> value="Pagari-ja-kondiitritooted">Хлебобулочные и кондитерские изделия</option>
                    <option <%
                        if (listindex.equals("Salatid-ja-valmistooted")) {
                            out.println("selected");
                        }
                        %> value="Salatid-ja-valmistooted">Салаты и готовые блюда</option>
                    <option <%
                        if (listindex.equals("Alkohol")) {
                            out.println("selected");
                        }
                        %> value="Alkohol">Алкогольные напитки</option>
                    <option <%
                        if (listindex.equals("Tubakatooted")) {
                            out.println("selected");
                        }
                        %> value="Tubakatooted">Табачные изделия</option>
                    <option <%
                        if (listindex.equals("Maiustused-ja-suupisted")) {
                            out.println("selected");
                        }
                        %> value="Maiustused-ja-suupisted">Сладости и закуски</option>
                    <option <%
                        if (listindex.equals("Konservid")) {
                            out.println("selected");
                        }
                        %> value="Konservid">Консервированые продукты</option>
                    <option <%
                        if (listindex.equals("Kulmutatud-tooted")) {
                            out.println("selected");
                        }
                        %> value="Kulmutatud-tooted">Замороженные продукты</option>
                    <option <%
                        if (listindex.equals("Tangud-pasta-maitseained-ja-kastmed")) {
                            out.println("selected");
                        }
                        %> value="Tangud-pasta-maitseained-ja-kastmed">Крупы макароны приправы и соусы</option>
                    <option <%
                        if (listindex.equals("Okotoit")) {
                            out.println("selected");
                        }
                        %> value="Okotoit">Экологическая еда и напитки</option>
                </select>
            </form>

            <br>
            <% if (request.getAttribute("output") != null) {
            %>
            <%= request.getAttribute("output")%> <%-- сокращенный способ вывести информацию на экран вместо out.println(); --%>
            <% }%>
        </div>
    </body>
</html>
