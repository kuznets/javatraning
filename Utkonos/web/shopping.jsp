
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping Page</title>
        <link href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.2.0/css/foundation.css" rel="stylesheet">
    </head>
    <body>

        <a href="index.jsp" target="_blank"> <button class="button large radius">Вернуться на страницу поиска</button></a>
        <br>

            <%
                String filepath = application.getRealPath("/") + "utkonos.txt";
                String fileData = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
            %>
            
        <div style=" text-align: left; width: 55%; margin-left: auto; margin-right: auto;">
            <%= fileData%>
        </div>

        <form action="">
            <input type="hidden" name="cleanall" value="yes" />
            <input class="button large radius" type="submit" value="Очистить список" />
        </form>
        <%
            if (request.getParameter("cleanall") != null) {
                synchronized (this) {
                    Files.write(Paths.get(filepath), "".getBytes());
                }

                response.sendRedirect("http://localhost:8080/Utkonos/shopping.jsp");
            }
        %>
    </body>
</html>
