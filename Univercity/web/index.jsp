<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Univercity Page</title>
    </head>
    <body>
        <form action="main" method="GET">
            <select name="format">
                <option>text</option>
                <option selected>html</option>
                <option>xml</option>
                <option>json</option>
                <option>sql</option>
                <option>jsp</option>
            </select>
            <br>
            <table border=0>
                <tr>
                    First Name:  
                <input type="text" name="firstname" value="" placeholder="Enter First Name" /> 
                </tr>
                <tr>
                    <td>Last Name: </td>
                    <td><input type="text" name="lastname" value="" placeholder="Enter Last Name"/></td> 
                </tr>
                <tr>
                    <td>Age: </td>
                    <td><input type="text" name="age" value="" placeholder="Enter Age"/> </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit" /> </td>
                </tr>
                <table/>
        </form>
        <%--сделать условие для university и проверить цикл--%>


        <c:out value="${university.name}"/><br>
        <c:out value="${university.address.city}"/><br>
        <c:out value="${university.score}"/><br>
        <c:out value="${university.listofstudents}"/><br> 

        <c:if test="${not empty xmlout}" >
            <hr>
            <h1>XML Output</h1>
            <x:parse xml="${xmlout}" var="data" />

            <x:out select="$data/university/name" /> <br>
            <x:out select="$data/university/address/city" /> <br>

            <x:out select="$data/university/listofstudents/entry[1]/value/firstname" /> <br>
            <x:out select="$data/university/listofstudents/entry[1]/value/address/city" /> <br>

        </c:if>

    </body>
</html>
