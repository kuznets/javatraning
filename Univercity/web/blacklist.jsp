<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Files"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%!
            private String filepath = "", fileData = "";
        %>
        <%
            request.setCharacterEncoding("utf-8");
            filepath = application.getRealPath("/") + "files\\blacklist.txt";
            fileData = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);//читаем файл
        %>

        <form method="post" action="" style="float: left;">
            <textarea name="blacklist" cols="40" rows="15"><%= fileData%></textarea>
            <br>
            <input type="image" title="Submit Changes"
                   src="images/btn_submit.gif" />
        </form>
            
        <%
            if (request.getParameter("blacklist") != null) {
                Files.write(Paths.get(filepath), request.getParameter("blacklist").getBytes("utf-8"));//перезаписываем файл
                response.sendRedirect("http://localhost:8080/Univercity/blacklist.jsp");
            }
        %>
     
        <%            
            String data = "";
            if (fileData != null) {
                data = fileData.replaceAll("\r\n", "\n").replaceAll("\n", "<br>");
            }
        %>
        <div style="float: left;"><%= data%></div>
        <div style="clear: both;"></div>
        <hr>
        <form method="post" action="" style="float: left;">
            <textarea name="list" cols="40" rows="15">qqqqq
wwwww
            </textarea>
            <br>
            <input type="image" title="Submit Changes"
                   src="images/btn_submit.gif" />
        </form>
        <%
        if (request.getParameter("list") != null) {
            data = request.getParameter("list");
            String arr[] = data.split("\r");
            ArrayList inputList = new ArrayList();
            for (String d : arr) {
                inputList.add(d);
            }

            ArrayList blackList = getBlackList();
            Iterator<String> blackListiter = blackList.iterator();

            while (blackListiter.hasNext()) {
                String test = blackListiter.next();
                Iterator<String> inputListiter = inputList.iterator();
                while (inputListiter.hasNext()) {
                    if (inputListiter.next().contains(test)) {
                        inputListiter.remove();
                    }
                }
            }

            Iterator<String> inputListiter = inputList.iterator();
        %>
        <div style="float: left;">
            <%
            while (inputListiter.hasNext()) {
            %>
            <%= inputListiter.next() + "<br>"%>
            <%}%>
        </div>
        <div style="clear: both;"></div>
        <%}%>
        
        <%!
            private ArrayList<String> getBlackList() {

                ArrayList<String> list = new ArrayList();

                if (!fileData.equals("")) {
                    String data = fileData.replaceAll("\r", "\n").replaceAll("\n+", "\n");
                    String[] u = data.split("\n");
                    for (int i = 0; i < u.length; i++) {
                        if (!u[i].equals("")) {
                            list.add(u[i]);
                        }
                    }
                }

                return list;
            }
        %>
    </body>
</html>
