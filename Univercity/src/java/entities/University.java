package entities;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class University {

    private int score = 10;
    private String name = "Stanford University";
    private Address address;
    private Map<UUID, Student> listofstudents = new HashMap<>();

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Map<UUID, Student> getListofstudents() {
        return listofstudents;
    }

    public void setListofstudents(Map<UUID, Student> listofstudents) {
        this.listofstudents = listofstudents;
    }

    
}
