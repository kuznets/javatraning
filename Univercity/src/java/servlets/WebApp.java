package servlets;

import entities.Address;
import entities.Student;
import entities.University;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@WebServlet("/main")
public class WebApp extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        University university = buildUniversity(); //наполняем данными о объекте
        String xmlOut = getXML(university);
        request.setAttribute("xmlout", xmlOut);
        request.setAttribute("university", university);
       
        RequestDispatcher rd = null;
        String format = request.getParameter("format");
        switch (format) {
            case "text":
                rd = request.getRequestDispatcher("/TextServlet");
                break;
            case "html":
                rd = request.getRequestDispatcher("/HtmlServlet");
                break;
            case "xml":
                rd = request.getRequestDispatcher("/XMLServlet");
                break;
            case "json":
                rd = request.getRequestDispatcher("/JsonServlet");
                break;
            case "sql":
                rd = request.getRequestDispatcher("/SqlServlet");
                break;
            case "jsp":
//                rd = request.getRequestDispatcher("/JspServlet");
                rd = request.getRequestDispatcher("/index.jsp");
                break;
            default:
                rd = request.getRequestDispatcher("/index.jsp");
                break;
        }

        rd.forward(request, response);

    }

    private String getXML(University university) {
        String xml = "";
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(University.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
//                    jaxbMarshaller.marshal(allresults, new File(path + "data.xml"));
//                    jaxbMarshaller.marshal(university, System.out);
            StringWriter stringWriter = new StringWriter();
            jaxbMarshaller.marshal(university, stringWriter);
            xml = stringWriter.toString();

        } catch (JAXBException ex) {
            ex.printStackTrace(System.err);
            Logger.getLogger(WebApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xml;
    }

    private University buildUniversity() {

        University university = new University();
        university.setAddress(new Address("Palo Alto", "CA", "Street", "95014"));
        Map<UUID, Student> listofstudents = university.getListofstudents();
        Student student = new Student("John", "Obama", 27);
        student.setAddress(new Address("New York", "CA", "Street", "12345"));
        Student student1 = new Student("Janet", "Yackson", 21);
        student1.setAddress(new Address("Boston", "Texas", "Street", "12345"));

        listofstudents.put(student.getUserid(), student);
        listofstudents.put(student1.getUserid(), student);

        return university;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
