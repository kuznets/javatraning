package servlets;

import entities.University;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@WebServlet("/XMLServlet")
public class XMLServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* TODO output your page here. You may use following sample code. */
        if (request.getAttribute("university") != null) {
            University university = (University) request.getAttribute("university");

            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(University.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
//                    jaxbMarshaller.marshal(allresults, new File(path + "data.xml"));
//                    jaxbMarshaller.marshal(university, System.out);
                response.setContentType("text/xml;charset=UTF-8");

                try (PrintWriter out = response.getWriter();) {
                    jaxbMarshaller.marshal(university, out);
                }
            } catch (JAXBException ex) {
                ex.printStackTrace(System.err);
                Logger.getLogger(XMLServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

@Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
