package servlets;

import entities.Student;
import entities.University;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TextServlet", urlPatterns = {"/TextServlet"})
public class TextServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getAttribute("univercity") != null) {
            University university = (University) request.getAttribute("univercity");

            response.setContentType("text/plain;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.println(university.getName()
                        + university.getScore() + "\n\n");
                out.println(university.getAddress());
                Map<UUID, Student> listofstudents = university.getListofstudents();
                Iterator<Student> iterator = listofstudents.values().iterator();

                while (iterator.hasNext()) {
                    out.println(iterator.next() + "\n");
                }

            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
