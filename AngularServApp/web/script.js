angular.module("mainModule", [])
        .controller("mainController", function ($scope, $http, jsonFilter)
        {
            $scope.person1 = {};
            $scope.person2 = {};
            $scope.person3 = {};

            $scope.submitData = function (person, resultVarName)
            {

//    var headers = {
//        "Custom-Header-1": "Custom header 1 value",
//        "My-Header-2": customHeaderFunc
//      };
// var params = {
//        param1: $scope.getParam1,
//        param2: $scope.getParam2
//      };
//      var config = {
//        headers: headers,
//        params: params
//      };
//      $http.get("server.php", config)
//        .success(function (data, status, headers, config)
//        {
//          $scope.getCallCustomHeadersResult = logResult("GET SUCCESS", data, status, headers, config);
//        })

//           $http({
//               method: 'post', 
//               url: "/AngServerApp_2/user", 
//               params: {firstName: $scope.person2.firstName, lastName: $scope.person2.lastName},
//               headers: {"X-Requested-With": "XMLHttpRequest"}
//           })

                var params = {
                    person: person
                };

//        $http.post("/AngularServApp/userservlet2", //params 
//                {
//                    data: person,
//                   //params: {firstName: $scope.person2.firstName, lastName: $scope.person2.lastName},
//                    // Passing the headers to the server for some prcessing
//                   headers: {"X-Requested-With": "XMLHttpRequest"} 
//                }
//                        )

                $http({
                    method: 'post',
                    url: "/AngularServApp/userservlet5",
                    data: params, // userservlet5
//                            data: {'firstName': $scope.person3.firstName, 'lastName': $scope.person3.lastName},
//                    data: person,
//                    params: {'firstName': $scope.person2.firstName, 'lastName': $scope.person2.lastName},
                    headers: {"X-Requested-With": "XMLHttpRequest"}

                })
                        .success(function (data, status, headers, config) {
                            $scope[resultVarName] = logResult("SUCCESS", data, status, headers, config);
                            ;
                        })
                        .error(function (data, status, headers, config) {
                            alert("failure\n\n" + status);
                            $scope[resultVarName] = logResult("SUBMIT ERROR", data, status, headers, config);
                        });

            };

            var logResult = function (str, data, status, headers, config)
            {
                return str + "\n\n" +
                        "data: " + data + "\n\n" +
                        "status: " + status + "\n\n" +
                        "headers: " + jsonFilter(headers()) + "\n\n" +
                        "config: " + jsonFilter(config);
            };

        });
