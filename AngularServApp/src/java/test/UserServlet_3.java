package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.Enumeration;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebServlet("/userservlet3")
public class UserServlet_3 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String str, firstName = "", lastName = "";
        BufferedReader br;

        Enumeration<String> headers = request.getHeaders("X-Requested-With");

        String header = request.getHeader("X-Requested-With");

        int here = 0;
        if ("XMLHttpRequest".equalsIgnoreCase(header)) {

            if (request.getMethod().equalsIgnoreCase("get")) {
                firstName = request.getParameter("firstName");
                lastName = request.getParameter("lastName");
                here = 0;
            } else if (request.getMethod().equalsIgnoreCase("post")) {
                br = request.getReader();
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }
                ///// alternative
                firstName = request.getParameter("firstName");
                lastName = request.getParameter("lastName");
                here = 0;
            }
        }

//        JsonReader reader = Json.createReader(new StringReader("{[]}"));
//        JsonArray jsonArray = reader.readArray();
//        reader.close();

        JsonObject jsonObject = Json.createReader(new StringReader(sb.toString())).readObject();
        Person person = new Person(jsonObject.getString("firstName"), jsonObject.getString("lastName"));

        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (!firstName.equals("") && !lastName.isEmpty()) {
                out.write("A new user has been created:\n\n" + jsonObject);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
