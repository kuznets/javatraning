package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebServlet("/userservlet5")
public class UserServlet_5 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String str, firstName = "", lastName = "";
        BufferedReader br;
        int here = 0;

        if (request.getMethod().equalsIgnoreCase("get")) {
            firstName = request.getParameter("firstName");
            lastName = request.getParameter("lastName");
            here = 0;
        } else if (request.getMethod().equalsIgnoreCase("post")) {

            br = request.getReader();
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            firstName = request.getParameter("firstName");
            lastName = request.getParameter("lastName");
        }

        Person person = null;

        JSONParser parser = new JSONParser();
        JSONObject jSONObject = null;
        try {
            jSONObject = (JSONObject) parser.parse(sb.toString());
            Map<String, String> map = (Map) jSONObject.get("person");
            person = new Person(map.get("firstName"), map.get("lastName"));
        } catch (ParseException e) {
            e.printStackTrace(System.err);
        }

        here = 0;

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

//        Type typeOf = new TypeToken<Map<String, Person>>() {
//            }.getType(); 
//        try {
//           Map<String, Person> map  = gson.fromJson(sb.toString(), typeOf);
//            person = map.get("person");
//             firstName = person.getFirstName();
//        lastName = person.getLastName();
//        } catch (JsonSyntaxException | ClassCastException e) {
//            e.printStackTrace(System.err);
//        }
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (person != null) {
                out.print("A new user has been created:\n\n"
                        + gson.toJson(person));
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
