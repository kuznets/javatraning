package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/userservlet4")
public class UserServlet_4 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String str, firstName = "", lastName = "";
        BufferedReader br;
         int here = 0;

            if (request.getMethod().equalsIgnoreCase("get")) {
                firstName = request.getParameter("firstName");
                lastName = request.getParameter("lastName");
                here = 0;
            } else if (request.getMethod().equalsIgnoreCase("post")) {
               
                br = request.getReader();
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }
                 firstName = request.getParameter("firstName");
                lastName = request.getParameter("lastName");
            }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type typeOf = new TypeToken<Person>() {
        }.getType();
        Person person = null;
        try {
            person = gson.fromJson(sb.toString(), new TypeToken<Person>() {
            }.getType());
        } catch (JsonSyntaxException | ClassCastException e) {
            e.printStackTrace(System.err);
        }

        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
//            if (!firstName.equals("") && !lastName.isEmpty()) {
                out.print("A new user has been created:\n\n" + 
                        gson.toJson(person));
//            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
