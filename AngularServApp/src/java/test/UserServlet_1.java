package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.json.Json;// GlassFish
import javax.json.JsonObject;// GlassFish

@WebServlet("/userservlet1")
public class UserServlet_1 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder sb = new StringBuilder();
        String str, firstName = "", lastName = "";
        BufferedReader br;
        int here;

        if (request.getMethod().equalsIgnoreCase("get")) {
            firstName = request.getParameter("firstName");
            lastName = request.getParameter("lastName");
            here = 0;
        } else if (request.getMethod().equalsIgnoreCase("post")) {
//            firstName = request.getParameter("firstName");
//            lastName = request.getParameter("lastName");
            here = 0;
            br = request.getReader();
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            here = 0;
//            firstName = request.getParameter("firstName");
//            lastName = request.getParameter("lastName");
//             here = 0;
        }
        
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .build();

        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (!firstName.equals("") && !lastName.isEmpty()) {
                out.write("A new user has been created:\n\n" + jsonObject);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
