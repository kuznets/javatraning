angular.module("mainModule", [])

        .controller('mainController', function ($scope) {
            $scope.closePopUpDialog = function () {
                $scope.showLoginPopUpDialog = false;
                $scope.showForgotPwdForm = false;
            };

            $scope.langArray = ["English", "Русский", "Eesti"];

            $scope.onLoginFrame = function () {
                $scope.showLoginPopUpDialog = true;
            };
            $scope.onForgotPwdForm = function () {
                $scope.showForgotPwdForm = true;
            };
        })

        .directive('login', function () {
            return {
                templateUrl: 'views/header/loginFrame.html', //вызвращаемый теплейт
                controller: function ($scope) {
                    $scope.showLoginPopUpDialog = false;
                    $scope.showForgotPwdForm = false;
                }
            };
        });