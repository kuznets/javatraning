angular.module("mainModule", [])
        .controller("mainController", function ($scope, $filter, dateFilter)
        {
            $scope.stringData = "Example string";
            $scope.dateData = new Date();
            $scope.numberData = 1350620.547;

            $scope.formatDate1 = function (date, format) {
                return $filter("date")(date, format);
            };

            $scope.formatDate2 = function (date, format) {
                return dateFilter(date, format);
            };
        })
        
        .filter("customCurrency", function (numberFilter)//numberFilter - angular filter by default
        {
            function isNumeric(value)
            {
                return (!isNaN(parseFloat(value)) && isFinite(value));
            }

            return function (inputNumber, currencySymbol, decimalSeparator, thousandsSeparator, decimalDigits) {
                if (isNumeric(inputNumber))
                {
                    currencySymbol = (typeof currencySymbol === "undefined") ? "$" : currencySymbol; //if() ? "result" : "else result"
                    decimalSeparator = (typeof decimalSeparator === "undefined") ? "." : decimalSeparator;
                    thousandsSeparator = (typeof thousandsSeparator === "undefined") ? "," : thousandsSeparator;
                    decimalDigits = (typeof decimalDigits === "undefined" || !isNumeric(decimalDigits)) ? 2 : decimalDigits;

                    if (decimalDigits < 0)
                        decimalDigits = 0;


                    var formattedNumber = numberFilter(inputNumber, decimalDigits);

                    // Extract the integral and the decimal parts
                    var numberParts = formattedNumber.split(".");

                    // Replace the "," symbol in the integral part
                    // with the specified thousands separator.
                    numberParts[0] = numberParts[0].split(",").join(thousandsSeparator);

                    var result = currencySymbol + numberParts[0];

                    if (numberParts.length == 2)
                    {
                        result += decimalSeparator + numberParts[1];
                    }

                    return result;
                } else
                {
                    return inputNumber;
                }
            };
        })
        
.filter('reverse', function () {
            return function (items) {
                return items.slice().reverse();
            };
        });
        
//        <tr ng-repeat="s in searches| reverse">
//        <td><b>{{s.query}}</b></td>

//     getData(array1, array2);   
//        $scope.q = $filter('filter')(a, b);

