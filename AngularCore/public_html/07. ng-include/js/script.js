var filterDemo = angular.module('filterDemo', []);


filterDemo.controller('tabsController', function ($scope) {
    $scope.tabs = [
        {
            name: 'Angular',
            url: 'tabs-data/angular.html',
            active1: true
        }, {
            name: 'Backbone',
            url: 'tabs-data/backbone.html',
            active1: false
        }, {
            name: 'Knockout',
            url: 'tabs-data/knockout.html',
            active1: false
        }
    ];


    $scope.tabaddress = 'tabs-data/angular.html';
    $scope.current = 'Angular';

    $scope.toggleTab = function (s) {
        $scope.tabaddress = s.url;
        $scope.current = s.name;
    };
});