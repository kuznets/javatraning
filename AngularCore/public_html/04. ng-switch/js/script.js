var showHideDemo = angular.module('showHideDemo',[]);
showHideDemo.controller('showHideController', function($scope){
	$scope.libs = [
		{name: 'AngularJS', checked: false},
		{name: 'Backnone', checked: true},
		{name: 'Knockout', checked: false},
		{name: 'Jquery', checked: false},
		{name: 'MooTools', checked: false},
		{name: 'YUI Library', checked: false},
		{name: 'Ember', checked: true}
	];
	
	$scope.markIt = function(s){
		s.checked = !s.checked;
	}
	
	$scope.allMarked = function(){
		var checkedItem = 0;
		angular.forEach($scope.libs, function(s){
			if(s.checked){
				checkedItem ++;
			}
		});
		if (checkedItem == $scope.libs.length){
				return true;
		}
	}
});