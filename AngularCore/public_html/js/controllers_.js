
angular.module('angTest.controllers', [])







.controller('ShwHideAnimateController', function($scope) {
  
  // set the default states
  $scope.lions = false;
  $scope.cranes = false;
})

        .controller("LoginController", function($scope, $location, AuthenticationFactory) {
            $scope.credentials = {username: "", password: ""};

            $scope.login = function() {
                AuthenticationFactory.login($scope.credentials);
            };
        })

        .controller("HomeController", function($scope, AuthenticationFactory) {
            $scope.title = "Select from the Report Types:";
            $scope.message = "Mouse over the images";

            $scope.logout = function() {
                AuthenticationFactory.logout();
            }
        })


.controller('general7Controller', function($scope) {

            $scope.allflowers = [{name: 'rose', price: 22}, {name: 'daizy', price: 33}, {name: 'tullip', price: 66}];

            $scope.addFlower = function() {
                $scope.allflowers.push({name: 'daizy'}, {name: $scope.flower_name});
            }
            $scope.switchActive = function(s) {
                s.active = !s.active;
            }
            $scope.total = function() {
                var total = 0;
                angular.forEach($scope.allflowers, function(s) {
                    if (s.active) {
                        total += s.price;
                    }
                });

                return total;
            }

        })

        .controller('general8Controller', function($scope, additionService) {

            $scope.total = 0;
            $scope.add = function() {
$scope.total = additionService.add($scope.number1, $scope.number2);
            };
        })

//This controller retrieves data from the customersService and associates it with the $scope
//The $scope is ultimately bound to the customers view
        .controller('CustomersController', function($scope, customersService) {

            //I like to have an init() for controllers that need to perform some initialization. Keeps things in
            //one place...not required though especially in the simple example below
            init();

            function init() {
                $scope.customers = customersService.getCustomers();
            }

            $scope.insertCustomer = function() {
                var firstName = $scope.newCustomer.firstName;
                var lastName = $scope.newCustomer.lastName;
                var city = $scope.newCustomer.city;
                customersService.insertCustomer(firstName, lastName, city);
                $scope.newCustomer.firstName = '';
                $scope.newCustomer.lastName = '';
                $scope.newCustomer.city = '';
            };

            $scope.deleteCustomer = function(id) {
                customersService.deleteCustomer(id);
            };
        })

//This controller retrieves data from the customersService and associates it with the $scope
//The $scope is bound to the order view
        .controller('CustomerOrdersController', function($scope, $routeParams, customersService) {
            $scope.customer = {};
            $scope.ordersTotal = 0.00;

            //I like to have an init() for controllers that need to perform some initialization. Keeps things in
            //one place...not required though especially in the simple example below
            init();

            function init() {
                //Grab customerID off of the route        
                var customerID = ($routeParams.customerID) ? parseInt($routeParams.customerID) : 0;
                if (customerID > 0) {
                    $scope.customer = customersService.getCustomer(customerID);
                }
            }

        })

//This controller retrieves data from the customersService and associates it with the $scope
//The $scope is bound to the orders view
        .controller('OrdersController', function($scope, customersService) {
            $scope.customers = [];

            //I like to have an init() for controllers that need to perform some initialization. Keeps things in
            //one place...not required though especially in the simple example below
            init();

            function init() {
                $scope.customers = customersService.getCustomers();
            }
        })

        .controller('NavbarController', function($scope, $location) {
            $scope.getClass = function(path) {
                if ($location.path().substr(0, path.length) == path) {
                    return true
                } else {
                    return false;
                }
            }
        })

//This controller is a child controller that will inherit functionality from a parent
//It's used to track the orderby parameter and ordersTotal for a customer. Put it here rather than duplicating 
//setOrder and orderby across multiple controllers.
        .controller('OrderChildController', function($scope) {
            $scope.orderby = 'product';
            $scope.reverse = false;
            $scope.ordersTotal = 0.00;

            init();

            function init() {
                //Calculate grand total
                //Handled at this level so we don't duplicate it across parent controllers
                if ($scope.customer && $scope.customer.orders) {
                    var total = 0.00;
                    for (var i = 0; i < $scope.customer.orders.length; i++) {
                        var order = $scope.customer.orders[i];
                        total += order.orderTotal;
                    }
                    $scope.ordersTotal = total;
                }
            }

            $scope.setOrder = function(orderby) {
                if (orderby === $scope.orderby)
                {
                    $scope.reverse = !$scope.reverse;
                }
                $scope.orderby = orderby;
            };

        })


        .controller('DisplayFeedsController', function($scope, $http, $timeout, $filter) {

            function storyInCollection(story) {
                for (var i = 0; i < $scope.stories.length; i++) {
                    if ($scope.stories[i].id === story.id) {
                        return true;
                    }
                }
                return false;
            }

            function addStories(stories) {
                var changed = false;
                angular.forEach(stories, function(story) {
                    if (!storyInCollection(story)) {
                        $scope.stories.push(story);
                        changed = true;
                    }
                });

                if (changed) {
                    $scope.stories = $filter('orderBy')($scope.stories, 'date');
                }
            }

            $scope.refreshInterval = 60;
            $scope.feeds = [{
                    url: 'http://dailyjs.com/atom.xml'
                }];
            $scope.stories = [];

            $scope.fetchFeed = function(feed) {
                feed.items = [];

                var apiUrl = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%3D'";
                apiUrl += encodeURIComponent(feed.url);
                apiUrl += "'%20and%20itemPath%3D'feed.entry'&format=json&diagnostics=true&callback=JSON_CALLBACK";

                $http.jsonp(apiUrl).
                        success(function(data) {
                            if (data.query.results) {
                                feed.items = data.query.results.entry;
                            }
                            addStories(feed.items);
                        }).
                        error(function(data) {
                            console.error('Error fetching feed:', data);
                        });

                $timeout(function() {
                    $scope.fetchFeed(feed);
                }, $scope.refreshInterval * 1000);
            };

            $scope.addFeed = function(feed) {
                if (feed.$valid) {
                    // Copy this feed instance and reset the URL in the form
                    var newFeed = angular.copy(feed);
                    $scope.feeds.push(newFeed);
                    $scope.fetchFeed(newFeed);
                    $scope.newFeed.url = '';
                }
            };

            $scope.deleteFeed = function(feed) {
                $scope.feeds.splice($scope.feeds.indexOf(feed), 1);
            };

            $scope.fetchFeed($scope.feeds[0]);
        })


.controller('NavCtrl', ['$scope', '$location', function ($scope, $location) {
  $scope.breadcrumbs = [];
  $scope.menu =   [
        {text: 'HOME', href:'/app/index.html', children: [
            {text:'MANAGE Dashboard', href:'/dashb'}
          ]
          },
        {text: 'MANAGE', href:'/manage', children: [
          {text:'MANAGE PEOPLE', href:'/manage-people', children: [
              {text:'MANAGE STAFF', href:'/manage-staff'},
              {text:'MANAGE CLIENTS', href:'/manage-clients'}              
            ]}
        ]},
        {text: 'REPORTS', href:'/reports', children: [
          {text: 'REPORT NUMERO UNO', href: '#'},
          {text: 'REP NUMERO 2', href: '#', children: [{text:'Third Tier', href: '#'}, {text:'Another Third Tier', href: '#', children: [
              {text:'SUB SUB NAV', href:'#'}
            ]}]}
        ]},
        {text: 'MY INFO', href:'/my-info' },
      ]

  
}])


.controller('form-submitCtrl', function ($scope, $http, $log, promiseTracker, $timeout) {
    $scope.subjectListOptions = {
      'bug': 'Report a Bug',
      'account': 'Account Problems',
      'mobile': 'Mobile',
      'user': 'Report a Malicious User',
      'other': 'Other'
    };

    // Inititate the promise tracker to track form submissions.
    $scope.progress = promiseTracker();

    // Form submit handler.
    $scope.submit = function(form) {
      // Trigger validation flag.
      $scope.submitted = false;

      // If form is invalid, return and let AngularJS show validation errors.
      if (form.$invalid) {
        return;
      }

      // Default values for the request.
      var config = {
        params : {
          'callback' : 'JSON_CALLBACK',
          'name' : $scope.name,
          'email' : $scope.email,
          'subjectList' : $scope.subjectList,
          'url' : $scope.url,
          'comments' : $scope.comments
        },
      };

      // Perform JSONP request.
      var $promise = $http.jsonp('response.json', config)
        .success(function(data, status, headers, config) {
          if (data.status == 'OK') {
            $scope.name = null;
            $scope.email = null;
            $scope.subjectList = null;
            $scope.url = null;
            $scope.comments = null;
            $scope.messages = 'Your form has been sent!';
            $scope.submitted = false;
          } else {
            $scope.messages = 'Oops, we received your request, but there was an error processing it.';
            $log.error(data);
          }
        })
        .error(function(data, status, headers, config) {
          $scope.progress = data;
          $scope.messages = 'There was a network error. Try again later.';
          $log.error(data);
        })
        .finally(function() {
          // Hide status messages after three seconds.
          $timeout(function() {
            $scope.messages = null;
          }, 3000);
        });

      // Track the request and show its progress to the user.
      $scope.progress.addPromise($promise);
    };
  })
  

;