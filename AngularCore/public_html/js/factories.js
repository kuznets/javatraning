
angular.module('angTest.factories', [])

        .factory("AuthenticationFactory", function($location) {
  return {
    login: function(credentials) {
      if (credentials.username !== "qq" || credentials.password !== "qq") {
        alert("Username must be 'qq', password must be 'qq'");
      } else {
        $location.path('/home');
      }
    },
    logout: function() {
      $location.path('/login');
    }
  };
})

.factory('customersFactory', function () {
        var factory = {};
        var customers = [
			{ name: 'Dave Jones', city: 'Phoenix' },
			{ name: 'Jamie Riley', city: 'Atlanta' },
			{ name: 'Heedy Wahlin', city: 'Chandler' },
			{ name: 'Thomas Winter', city: 'Seattle' }
        ];

        factory.getCustomers = function () {
            //Can use $http object to retrieve remote data
            //in a "real" app
            return customers;
        };
        return factory;
    })
    
    
        .factory('FeedService',['$http',function($http){
    return {
        parseFeed : function(url){
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    }
}]);
    
;
