angular.module('angTest.controllers', [])

        .controller('general3Controller', function ($scope) {
            $scope.names = ['Dave', 'Йцукен', 'Abdul', 'Владимир'];
            $scope.test = 'qwerty';
        })

        .controller('general4Controller', function ($scope) {
            $scope.customers = [
                {name: 'Dave Jones', city: 'Phoenix'},
                {name: 'Jamie Riley', city: 'Atlanta'},
                {name: 'Heedy Wahlin', city: 'Chandler'},
                {name: 'Thomas Winter', city: 'Seattle'}
            ];

            $scope.addCustomer = function () {
                $scope.customers.push({name: $scope.inputData.name, city: $scope.inputData.city});
                $scope.inputData = {};
            };
        })

        .controller('general6Controller', function ($scope) {

            $scope.customers = [
                {name: 'Dave Jones', city: 'Phoenix'},
                {name: 'Jamie Riley', city: 'Atlanta'},
                {name: 'Heedy Wahlin', city: 'Chandler'},
                {name: 'Thomas Winter', city: 'Seattle'}
            ];

            $scope.addCustomer = function () {
                $scope.customers.push(
                        {
                            name: $scope.inputData.name,
                            city: $scope.inputData.city
                        });
                $scope.inputData = {};
            };

            $scope.removeCustomer = function (x, y) {
                $scope.customers.splice(x, y);
            };

        })
// http://www.w3schools.com/js/js_array_methods.asp
// http://www.w3schools.com/js/js_array_sort.asp
        .controller('general7Controller', function ($scope, additionService) {

            $scope.total = 0;
            $scope.add = function () {
                $scope.total = additionService.add($scope.number1, $scope.number2);
            };
        })

        .controller('general8Controller', function ($scope) {

            $scope.allflowers = [{name: 'rose', price: 22}, {name: 'daizy', price: 33}, {name: 'tullip', price: 66}];

            $scope.addFlower = function () {
                $scope.allflowers.push({name: $scope.flower_name, price: $scope.flower_price});
            };
            $scope.switchActive = function (s) {
                s.active = !s.active;
            };

            $scope.total = function () {
                var total = 0;
                angular.forEach($scope.allflowers, function (s) {
                    if (s.active) {
                        total += s.price;
                    }
                });

                return total;
            };

        });
