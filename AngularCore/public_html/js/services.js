
angular.module('angTest.services', [])

.service("additionService", function(){
    this.add = function(a, b){
        return a + b;
    };
});
