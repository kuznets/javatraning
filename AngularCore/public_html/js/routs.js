
angular.module('angTest.routs', [])

        .config(function ($routeProvider) {

            var urlbase = 'partials/';

            $routeProvider.when('/general1', {
                templateUrl: urlbase + 'general1.html'
            })

                    .when('/general2', {
                        templateUrl: urlbase + 'general2.html'
                    })

                    .when('/general3', {
                        templateUrl: urlbase + 'general3.html',
                        controller: 'general3Controller'
                    })

                    .when('/general4', {
                        templateUrl: urlbase + 'general4.html',
                        controller: 'general4Controller'
                    })

                    .when('/general5', {
                        templateUrl: urlbase + 'general5.html',
                        controller: 'general4Controller'
                    })

                    .when('/general6', {
                        templateUrl: urlbase + 'general6.html',
                        controller: 'general6Controller'
                    })

                    .when('/general7', {
                        templateUrl: urlbase + 'general7.html',
                        controller: 'general7Controller'
                    })

                    .when('/general8', {
                        templateUrl: urlbase + 'general8.html',
                        controller: 'general8Controller'
                    })

                    .otherwise({redirectTo: '/general1'});

        });
