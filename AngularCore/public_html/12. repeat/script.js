angular.module("mainModule", [])
        .controller("mainController", function ($scope)
        {

            $scope.areAllPeopleSelected = false;

            $scope.people = [
                {firstName: "John", lastName: "Doe"},
                {firstName: "Bob", lastName: "Smith"},
                {firstName: "Jack", lastName: "White"},
                {firstName: "Michael", lastName: "Green"}
            ];

            $scope.selectablePeople = [
                {firstName: "John", lastName: "Doe", isSelected: false},
                {firstName: "Bob", lastName: "Smith", isSelected: false},
                {firstName: "Jack", lastName: "White", isSelected: false},
                {firstName: "Michael", lastName: "Green", isSelected: false}
            ];

            $scope.cars = [
                {carBrand: "MB", carModel: "124"},
                {carBrand: "BMW", carModel: "725"},
                {carBrand: "Opel", carModel: "Vectra"},
                {carBrand: "Citroen", carModel: "C5"}
            ];

            $scope.model = {
                selectedLabelList: []
            };

            $scope.stringsArray = [];
            var currStringIndex = 0;


            $scope.updatePeopleSelection = function (peopleArray, selectionValue) {
                for (var i = 0; i < peopleArray.length; i++)
                {
                    peopleArray[i].isSelected = selectionValue;
                }
            };

            $scope.isSelectedAll = function () {
                $scope.model.selectedLabelList = [];

                if ($scope.areAllIsSelected) {
                    $scope.areAllIsSelected = true;
                    for (var i = 0; i < $scope.cars.length; i++) {
                        $scope.model.selectedLabelList.push($scope.cars[i].carBrand);
                        //$scope.model.selectedLabelList.push($scope.cars[i].carModel);
                    }
                } else {
                    $scope.areAllIsSelected = false;
                }

                angular.forEach($scope.cars, function (item) {
                    item.selected = $scope.areAllIsSelected;
                });
            };

            $scope.isLabelChecked = function ()
            {
                var _carBrand = this.car.carBrand;
                if (this.car.selected) {
                    $scope.model.selectedLabelList.push(_carBrand);
                    if ($scope.model.selectedLabelList.length == $scope.labelList.length)
                    {
                        $scope.areAllIsSelected = true;
                    }
                } else {
                    $scope.areAllIsSelected = false;
                    var index = $scope.model.selectedLabelList.indexOf(_carBrand);
                    $scope.model.selectedLabelList.splice(index, 1);
                }
            }

            $scope.getPersonPositionDesc = function (isFirst, isMiddle, isLast, isEven, isOdd) {
                var result = "";

                if (isFirst)
                {
                    result = "(first";
                } else if (isMiddle)
                {
                    result = "(middle";
                } else if (isLast)
                {
                    result = "(last";
                }

                if (isEven)
                {
                    result += "-even)";
                } else if (isOdd)
                {
                    result += "-odd)";
                }

                return result;
            };

            $scope.addStringToArray = function () {
                $scope.stringsArray.push("Item " + currStringIndex);
                currStringIndex++;
            };

            $scope.removeStringFromArray = function (stringIndex) {
                if (stringIndex >= 0 && stringIndex < $scope.stringsArray.length)
                {
                    $scope.stringsArray.splice(stringIndex, 1);
                }
            };
        });