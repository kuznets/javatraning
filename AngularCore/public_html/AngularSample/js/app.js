angular.module('app', ['ngRoute', 'ngSanitize'])

        .config(function ($routeProvider) {
            $routeProvider
                    .when('/home', {
                        template: '<h1>Welcome to our services!</h1>\n\
        <a href="#pricing">Place an order now!</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#list">View List</a>'
                    })
                    .when('/list', {
                        templateUrl: 'partials/list.html',
                        controller: 'listCtrl'
                    })
                    .when('/pricing', {
                        templateUrl: 'partials/pricing.html',
                        controller: 'pricingController'
                    })
                    .otherwise({redirectTo: '/home'});
        })

        .controller('pricingController', function ($scope) {
              $scope.plans = [
                {chosen: true, name: 'Best Service Pro', price: '$49.99/month', link: 'http://www.google.com/'},
                {name: 'Best Service Lite', price: '$24.99/month', link: 'http://www.yahoo.com/'},
            ];

            $scope.selectedplan = $scope.plans[0]; //Ставим по умолчанию выбранный объект

            $scope.switchChosen = function (s) {
                angular.forEach($scope.plans, function (p) {
                    if (p !== s) {
                        p.chosen = false;
                    } else {
                        p.chosen = true;
                    }
                });
            }
        })

       
        .controller('listCtrl', function ($scope, $http, $sce, $filter) {
            $scope.list = [];
            var url = "http://public-api.wordpress.com/rest/v1/sites/wtmpeachtest.wordpress.com/posts?callback=JSON_CALLBACK";
            $http.jsonp(url)
                    .success(function (data) {
                        $scope.list = data.posts;
                    });

            $scope.sanitize = function (snippet) {
                return $sce.trustAsHtml(snippet);
            };

            $scope.sortBy = function (column) {
                $scope.Header = ['', ''];
                $scope.columnToOrder = column;
                $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse_);

                var iconName;
                if ($scope.reverse_)
                    iconName = 'glyphicon glyphicon-chevron-up';
                else
                    iconName = 'glyphicon glyphicon-chevron-down';


                if (column === 'content')
                {
                    $scope.Header[0] = iconName;
                } else if (column === 'title')
                {
                    $scope.Header[1] = iconName;
                }

                $scope.reverse_ = !$scope.reverse_;
            };

            $scope.sortBy('title');

        });

