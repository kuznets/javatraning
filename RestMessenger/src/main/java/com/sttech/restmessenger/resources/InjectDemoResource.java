package com.sttech.restmessenger.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/injectdemo") // http://localhost:8084/RestMessenger/webapi/injectdemo
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.TEXT_PLAIN)
public class InjectDemoResource {

    @GET // http://localhost:8084/RestMessenger/webapi/injectdemo/
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessages() {
        return "Hello World11111!";
    }

    @GET
    @Path("annotations") // http://localhost:8084/RestMessenger/webapi/injectdemo/annotations ...
    public String getParamsUsingAnnotations(
            @MatrixParam("param") String matrixParam,
            @HeaderParam("header") String header,
            @CookieParam("name") String cookie)
    {
        return "Matrix param: " + matrixParam + " Header param: " + header + " Cookie param: " + cookie;
    }

    @GET
    @Path("context") // http://localhost:8084/RestMessenger/webapi/injectdemo/context ...
    public String getParamsUsingContext(@Context UriInfo uriInfo, @Context HttpHeaders headers) {

        String path = uriInfo.getAbsolutePath().toString();
        String header = headers.getRequestHeaders().toString();
//        String cookies = headers.getCookies().toString();
        return "Path : " + path + " Header: " + header; // + " Cookies: " + cookies;
    }

}
