package com.sttech.restmessenger.testresource;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("webapi")
public class AppConfig extends Application {

//    @Override
//    public Set<Class<?>> getClasses() {
//        Set<Class<?>> resources = new HashSet<>();
//        addRestResourceClasses(resources);
//        return resources;
//    }
//
//    private void addRestResourceClasses(Set<Class<?>> resources) {
//        resources.add(com.sttech.restmessenger.testresource.TestResource.class);
//        resources.add(com.sttech.restmessenger.services.MessageService.class);
//    }
}

//@ApplicationPath("webresources")
//public class ApplicationConfig extends Application {
//
//    @Override
//    public Set<Class<?>> getClasses() {
//        Set<Class<?>> resources = new HashSet<>();
//        addRestResourceClasses(resources);
//        return resources;
//    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
//    private void addRestResourceClasses(Set<Class<?>> resources) {
//        resources.add(messenger00.resources.MessageResource.class);
//        resources.add(messenger01.resources.MessageResource.class);
//        resources.add(messenger02.resources.MessageResource.class);
//        resources.add(messenger03.resources.MessageResource.class);
//        resources.add(messenger04.resources.MessageResource.class);
//        resources.add(messenger05.resources.MessageResource.class);
//        resources.add(messenger05.resources.ProfileResource.class);
//        resources.add(messenger06.resources.MessageResource.class);
//        resources.add(messenger06.resources.ProfileResource.class);
//        resources.add(messenger07.resources.InjectDemoResource.class);
//        resources.add(messenger07.resources.MessageResource.class);
//        resources.add(messenger07.resources.ProfileResource.class);
//        resources.add(messenger08.resources.InjectDemoResource.class);
//        resources.add(messenger08.resources.MessageResource.class);
//        resources.add(messenger08.resources.ProfileResource.class);
//        resources.add(messenger09.resources.CommentResource.class);
//        resources.add(messenger09.resources.InjectDemoResource.class);
//        resources.add(messenger09.resources.MessageResource.class);
//        resources.add(messenger09.resources.ProfileResource.class);
//        resources.add(messenger10.resources.CommentResource.class);
//        resources.add(messenger10.resources.InjectDemoResource.class);
//        resources.add(messenger10.resources.MessageResource.class);
//        resources.add(messenger10.resources.ProfileResource.class);
//        resources.add(messenger11.exception.DataNotFoundExceptionMapper.class);
//        resources.add(messenger11.resources.CommentResource.class);
//        resources.add(messenger11.resources.InjectDemoResource.class);
//        resources.add(messenger11.resources.MessageResource.class);
//        resources.add(messenger11.resources.ProfileResource.class);
//    }

