package com.sttech.restmessenger.testresource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/messages0")// http://localhost:8084/RestMessenger/webapi/messages0
public class TestResource {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getMessages() {
		return "Hello World11111!";
	}

}