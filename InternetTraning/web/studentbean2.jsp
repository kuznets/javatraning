
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Example</title>
    </head>
    <body>

        <p>Student First Name: 
            <jsp:getProperty name="students" property="firstName"/>
        </p>
        <p>Student Last Name: 
            <jsp:getProperty name="students" property="lastName"/>
        </p>
        <p>Student Age: 
            <jsp:getProperty name="students" property="age"/>
        </p>

    </body>
</html>