
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Example</title>
    </head>
    <body>
session request page application
        <jsp:useBean id="students" class="Entities.StudentBean" scope="session" > 
            <jsp:setProperty name="students" property="firstName"
                             value="Sergei"/>
            <jsp:setProperty name="students" property="lastName" 
                             value="Subbotin"/>
            <jsp:setProperty name="students" property="age" 
                             value="56"/>
        </jsp:useBean>

        <p>Student First Name: 
            <jsp:getProperty name="students" property="firstName"/>
        </p>
        <p>Student Last Name: 
            <jsp:getProperty name="students" property="lastName"/>
        </p>
        <p>Student Age: 
            <jsp:getProperty name="students" property="age"/>
        </p>

    </body>
</html>

<!--CREATE TABLE IF NOT EXISTS studentss (
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  age INT      
) engine=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE comments (id INT NOT NULL AUTO_INCREMENT, 
    MYUSER VARCHAR(30) NOT NULL,
    EMAIL VARCHAR(30), 
    WEBPAGE VARCHAR(100) NOT NULL, 
    DATUM DATE NOT NULL, 
    SUMMARY VARCHAR(40) NOT NULL,
    COMMENTS VARCHAR(400) NOT NULL,
    PRIMARY KEY (ID));

INSERT INTO comments values (default, 'lars', 'myemail@gmail.com','http://www.vogella.com', '2009-09-14 10:33:11', 'Summary','My first comment'); 
-->
