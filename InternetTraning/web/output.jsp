<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Java Lesson</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>
        <% String query = "Java developer";
            if (request.getAttribute("query") != null) {
                query = request.getAttribute("query").toString();
            }

        %>
        <form action="WebRouting">
            <input type="text" name="query" value="<%= query%>" /><br>
            <input type="text" name="query" value="${query}" /> <!-- Второй вариант -->
            <input type="submit" value="Start" />
        </form>

        <br><br>

        <c:if test="${not empty outdata.getListOfResults()}">
            <select>
                <c:forEach var="data" items="${outdata.getListOfResults()}">
                    <option><c:out value="${data.title}"/></option>
                </c:forEach>
            </select>
        </c:if>

        <br><br>
        <c:choose>
            <c:when test="${empty outdata}">
                No data yet...
            </c:when>
            <c:otherwise>
                <ul>
                    <c:forEach var="data" items="${outdata.getListOfResults()}">
                        <c:url value="${data.getLink()}" var="url" />
                        <li><a href="${url}" target=\"_blank\">${data.getTitle()}</a></li>           
                        </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>

    </body>
</html>
