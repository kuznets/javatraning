package Controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JsonUtil {

    private String path;

    public void setPath(String path) {
        this.path = path;
    }
    
    void serialize(SearchResult sr, String filename) {
        String custompath = path + filename;
//        Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(sr);

        try {
            Files.write(Paths.get(custompath), json.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(JsonUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void serialize2JSON (ArrayList list, String filename) {
        String custompath = path + filename;
//        Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(list);

        try {
            Files.write(Paths.get(custompath), json.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(JsonUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    ArrayList deserializeFromJSON(String filename) {

        filename = path + filename;
        Gson gson = new Gson();
        java.lang.reflect.Type typeOf = null;
       
            typeOf = new TypeToken<ArrayList<SearchResult>>() {
            }.getType();

        String fileData;
        try {
            fileData = new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            fileData = null;
        }

        ArrayList al = null;
        try {
            al = gson.fromJson(fileData, typeOf);
        } catch (JsonSyntaxException | ClassCastException e) {
            e.printStackTrace(System.err);
        }
        if (al == null) {
            al = new ArrayList();
        }

        return al;
    }
    
    public void serializeData (AllDataBuilder data) {
        String custompath = path + "allData.json";
//        Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(data);

        try {
            Files.write(Paths.get(custompath), json.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(JsonUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
