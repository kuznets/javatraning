package Controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class XmlUtil {
    
    private String path;

    public void setPath(String path) {
        this.path = path;
    }
    
    void makeXML(ArrayList<SearchResult> list) {

        AllDataBuilder allresults = new AllDataBuilder();
        allresults.getListOfResults().addAll(list);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(AllDataBuilder.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(allresults, new File(path + "data.xml"));
            jaxbMarshaller.marshal(allresults, System.out);
        } catch (JAXBException ex) {
            Logger.getLogger(XmlUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    ArrayList<SearchResult> unmarshall() {
        ArrayList<SearchResult> list = new ArrayList<>();
        try {
            File file = new File(path + "data.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(AllDataBuilder.class);// 

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AllDataBuilder all = (AllDataBuilder) jaxbUnmarshaller.unmarshal(file);
//            System.out.println( list );
            list = all.getListOfResults();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return list;
    }

    
}
