package Controllers;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebSearch {

    private String cookie;
    private String path;
    private JsonUtil jsonUtil;
    private XmlUtil xmlUtil;

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public void setPath(String path) {
        this.path = path;
    }

//    String readURL(String urlAddress) throws MalformedURLException, IOException {
//        StringBuilder urldata = new StringBuilder();
//        String newLine = System.lineSeparator();
//
//        URL url = new URL(urlAddress);
//        InputStream inputStream = url.openStream();
//        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) { //BufferedReader, InputStream, openStream почитать!
//            String line = "";
//            while (line != null) {
//                line = bufferedReader.readLine();
//                urldata.append(line).append(newLine);
//            }
//        }
//
//        return urldata.toString();
//    }
    //1 Метод получает поисковый запрос, формирует урл и передает его на парсировку
//    public ArrayList<SearchResult> getInputText(String inputText) {
    public AllDataBuilder getInputText() { //String inputText

//        ArrayList<SearchResult> listOfResult = jsonUtil.deserializeFromJSON("JsonDirdata.json"); ///new ArrayList();
//        ArrayList<SearchResult> listOfResult = new ArrayList();
        
        ArrayList pages = new ArrayList();

        createFolder("ResultDir", path);
        createFolder("JsonDir", path);
        createFolder("XmlDir", path);
        String separator = getSeparator();
        jsonUtil = new JsonUtil();
        jsonUtil.setPath(path + "JsonDir" + separator);
        xmlUtil = new XmlUtil();
        xmlUtil.setPath(path + "XmlDir" + separator);
        
        ArrayList<SearchResult> listOfResult = xmlUtil.unmarshall();

        String address = "";
        int pamount = 3;
        int num = 0;
        query = query.replaceAll(" ", "+");
        for (int i = 0; i < pamount; i++) {
            address = "http://go.mail.ru/search?q=" + query + "&num=10&rch=l&sf=" + num;
            num += 10;

            listOfResult.addAll(parser(fetchUrl(address, true))); //отправляем на парсировку
            int q = 0;
        }
        jsonUtil.serialize2JSON(listOfResult, "data.json");
        xmlUtil.makeXML(listOfResult);

//        return listOfResult;
        AllDataBuilder allResults = new AllDataBuilder();
        allResults.setListOfResults(listOfResult);

        jsonUtil.serializeData(allResults);
        return allResults; 
    }

    //2 Метод получает веб страницу с удаленного УРЛ
    private String fetchUrl(String urlAddress, boolean useCookie) {
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(urlAddress); //класс для работы в url адресами
            HttpURLConnection urlconnect = (HttpURLConnection) url.openConnection(); //соединение с удаленным url
            urlconnect.setRequestProperty("GET", "HTTP/1.1"); //передаем инфу о себе, как будто мы браузер
            urlconnect.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O;  en-US; rv:1.8.1.12) Gecko/20080201 Firefox/2.0.0.12");
            urlconnect.setRequestProperty("Content-Encoding", "UTF-8");
            urlconnect.setRequestProperty("Content-type", "text/html; charset=UTF-8");

            urlconnect.setConnectTimeout(5000);
            urlconnect.setReadTimeout(5000);
            urlconnect.setInstanceFollowRedirects(false);
            if (useCookie) {
                urlconnect.setRequestProperty("Cookie", cookie); //Передаем наши куки удаленному УРЛ
            }

            String line = "";
            String newLine = System.lineSeparator(); //Перевод каретки для нашей ОС
            int status = urlconnect.getResponseCode(); // получаем ответ, статус соединения от удаленного сервера
            //String message = urlconnect.getResponseMessage();

            if (status == 200) {
                MyCookies ccookie = new MyCookies();
                if (useCookie) {
                    ccookie.getCookieFromURL(urlconnect);
                    cookie = ccookie.getCookie();
                    int q = 0;
                }

                String contentType = urlconnect.getHeaderField("Content-Type");
                String charset = null;
                for (String param : contentType.replace(" ", "").split(";")) {
                    if (param.startsWith("charset=")) {
                        charset = param.split("=", 2)[1];
                        break;
                    }
                }
                if (charset == null) {
                    charset = "UTF-8";
                }

                InputStream is = urlconnect.getInputStream(); //открываем входной поток
                BufferedReader rdr = new BufferedReader(new InputStreamReader(is, charset)); //читаем текст в буфер из потока / декодируем байты в символы в заданной кодировке
                while ((line = rdr.readLine()) != null) {
                    sb.append(line);
                    sb.append(newLine);
                }
                urlconnect.disconnect();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(WebSearch.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

    //3 Метод получает текст веб страницы и осуществляет парсинг данных
    ArrayList<SearchResult> parser(String data) {
        ArrayList<SearchResult> result = new ArrayList<>();

        String[] searchBlocks = data.split("\" class=\"result__li\">");
        int q = 0;
        for (int i = 1; i < searchBlocks.length; i++) {

//            String lableStartTitle = "<b>";  //титл
            String lableStartDesc = "<div class=\"result__snp\">";  //описание
            String lableStartUrl = "light-link\" target=\"_blank\" href=\""; //URL
//            String lableEndTitle = "</a>"; //титл, описание
            String lableEndDesc = "</div>";
            String lableEndUrl = "\"";
            String textBlock = searchBlocks[i];
            textBlock = textBlock.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "")
                    .replaceAll(" +", " "); //чистим от мусора
//            int x = textBlock.indexOf(lableStart); //ищем начало ссылки
//            int y = textBlock.indexOf(lableEnd, x + lableStart.length()); //ищем конец ссылки
//            if (x > -1 && y > x) {
//                String link = textBlock.substring(x + lableStart.length(), y);
//                sr.setLink(link);
//                result.add(sr);
//            }
//            SearchResult sr = new SearchResult();
//            String resultTitle = parseHelper(textBlock, lableStartTitle, lableEndTitle);
//            resultTitle = resultTitle.replaceAll("<b>", "").replaceAll("</b>", "");
//            sr.setTitle(resultTitle);
            String resultDesc = parseHelper(textBlock, lableStartDesc, lableEndDesc);
            resultDesc = resultDesc.replaceAll("<b>", "").replaceAll("</b>", "").replaceAll("<span class=\"snsep\">", "").replaceAll("</span>", "");
//            sr.setDesc(resultDesc);
            String resultUrl = parseHelper(textBlock, lableStartUrl, lableEndUrl);
            if (!resultUrl.isEmpty()) {

//                sr.setLink(resultUrl);
                String webpage = fetchUrl(resultUrl, false); //Получаем тело удаленной родительской страници

                String resultTitle = getTitle(webpage); // получаем титлы с родительской страници  //проверить на 5 создании файлов
                resultTitle = resultTitle.replaceAll("/", "").replaceAll("&quot;", "").replaceAll("|", "").replaceAll("\r\n", "").replaceAll(":", "");

                while (resultTitle.startsWith(" ")) { //Не даем создавать файлы с пустыми символами в названии впереди
                    resultTitle = resultTitle.replaceFirst(" ", "");
                    q = 0;
                }

                SearchResult sr = new SearchResult(resultTitle, resultDesc, resultUrl);
                if (!resultTitle.equals("")) {
//                    saveToFile(resultTitle, "ResultDir", webpage);
                    saveToFile(resultTitle, "ResultDir", parseDocument(webpage));
                    result.add(sr);
                    
                Map m = mapResult.asMap();
//                SearchResult sr = listOfSearchResults.get(listOfSearchResults.size() - 1);
                sr.setResultsMap(m);                
                    
                    q = 0;

//                    if (sr != null) {
//                        datanumber++;
//                        jsonUtil.serialize(sr, "test_" + datanumber + ".json");
//                    }
                }
            }
        }
        q = 0;
        return result;
    }
    
    private String query;

    public void setQuery(String query) { //проверить что с плюсами а не пробелами
        this.query = query;
    }
    
    private Multimap<String, String> mapResult;
    
    private String highLight(String[] array, String sent) {
        String sent2 = sent.toLowerCase();
        TreeMap<Integer, Integer> map = new TreeMap();
        for (int i = 0; i < array.length; i++) {
            int begin = 0, from = 0;
            while (begin > -1) {
                begin = sent2.indexOf(array[i], from);
                from = begin + array[i].length();
                if (begin > -1) {
                    map.put(begin, from);
                }
            }
        }

        String before = "<b><font color=\"#2554C7\">", after = "</font></b>";
        int k = after.length() + before.length();
        int z = 0;
        Iterator<Integer> iter = map.keySet().iterator();
        while (iter.hasNext()) {
            int begin = iter.next();
            int end = map.get(begin) + z;
            begin += z;

            sent = sent.substring(0, begin) + before + sent.substring(begin, end) + after + sent.substring(end);

            z += k;
        }

        return sent;
    }

    
    private String parseDocument(String txt) {
        int start = txt.indexOf("<body");
        int end = txt.indexOf("</body>");
        if (start > -1 && end > start) {
            txt = txt.substring(start, end);
        }
        start = 0;
        int indexfrom = 0;
        while (start > -1) {
            start = txt.indexOf("<script", indexfrom);
            indexfrom = start + 1;
            if (start > -1) {
                end = txt.indexOf("</script>", start);
                if (end > start) {
                    txt = txt.substring(0, start) + txt.substring(end);
                }
            }

        }

        txt = txt.replaceAll("[.]<", ". <"); //[] = экранирование точки, в regex точка означает Все.

        txt = txt.replaceAll("<[^>]*>", " ").replaceAll(" +", " "); //[^>] = любой символ кроме >, * = любое колличество
        String txt1 = txt.toLowerCase();
        String search = query.toLowerCase();
        String[] array = search.split("[+]"); //[+] = из запроса, УРЛ строки, там разделение +
//        TreeMultimap<String, String> map = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        mapResult = HashMultimap.create();

        for (int i = 0; i < array.length; i++) {

            int x = 0, from = 0;
            while (x > -1) {
                x = txt1.indexOf(array[i], from);
                from = x + 1;

                if (x > -1) {
                    int y1 = txt1.lastIndexOf(". ", x);
                    if (y1 == -1) {
                        y1 = 0;
                    } else {
                        y1 += 2;
                    }

                    int y2 = txt1.indexOf(". ", x);
                    if (y2 == -1) {
                        y2 = txt1.length();
                    } else {
                        y2 += 1;
                    }
                    if (y2 > y1) {

                        String sentence = txt.substring(y1, y2);
                        if (sentence.length() < 300) {
                            mapResult.put(array[i], highLight(array, sentence));
                        }
                    }

                }
            }
        }

////        SearchResult sr = listOfSearchResults.get(listOfSearchResults.size()-1); //new SearchResult();
//        Map m = mapResult.asMap();
//        sr.setResultsMap(m);
//                
//        datanumber++;
//                jSONworker.serialize(sr, "test" + datanumber+ ".json");
//                + ".json");
//        jSONworker.serializeMap2JSON(map, "test" + datanumber + ".json");
        TreeMultimap<Integer, String> map2 = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        Iterator<String> itermapkey1 = mapResult.keySet().iterator();
        while (itermapkey1.hasNext()) {
            String key = itermapkey1.next();
            map2.put(mapResult.get(key).size(), key);
        }

        StringBuilder sb = new StringBuilder("<table style=\" text-align: left; width: 66%; "
                + "background-color:rgb(255, 255, 204); margin-left: auto; "
                + "margin-right: auto;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\">");// Создаем таблицу

        Iterator<Integer> iter = map2.keySet().iterator();
        while (iter.hasNext()) {
            int key = iter.next();
            SortedSet set = map2.get(key);
            Iterator<String> iter2 = set.iterator();
            while (iter2.hasNext()) {
                String wordofset = iter2.next();
                Collection set2 = mapResult.get(wordofset);
                Iterator<String> iter3 = set2.iterator();
                String sentenceofset2 = "";
                while (iter3.hasNext()) {
                    sentenceofset2 += iter3.next() + "<br><br>";
                }
                sb.append("<tr><td>").append(wordofset + " (" + key + ")").append("</td><td>").append(sentenceofset2).append("</td></tr>");
            }
        }

        sb.append("</table>");

        return sb.toString();

    }

//    int datanumber = 0;
    //4
    private String parseHelper(String textBlock, String lableStart, String lableEnd) {
        String resultHelper = "";
        int x = textBlock.indexOf(lableStart); //ищем начало ссылки
        int y = textBlock.indexOf(lableEnd, x + lableStart.length()); //ищем конец ссылки
        if (x > -1 && y > x) {
            resultHelper = textBlock.substring(x + lableStart.length(), y);
        }
        int q = 0;
        return resultHelper;
    }

    //5
    private String getTitle(String webpage) { //если webpage пустая, то result передаст пустой, обратить внимание!!!
        String result = "Untitled";
        result = parseHelper(webpage, "<title>", "</title>");
        if (result.isEmpty()) {
            result = parseHelper(webpage, "<Title>", "</Title>");
        }

        return result;
    }

    private void saveToFile(String filename, String foldername, String sourcetext) { //Файл называем титлами из поисковика, а потом получаем титлы страници исходника
        File file = new File(path + foldername + getSeparator() + filename + ".html");
        try {
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF-8")); //Посмотреть Files
            pw.write(sourcetext);

            pw.flush();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        int q = 0;
    }

    protected Boolean createFolder(String fname, String path) {
        String rootPath = path + fname;

        File dir = new File(rootPath);
        if (!dir.exists()) {
            System.out.println("Create: " + dir);
            dir.mkdir();
            return true;
        } else {
//            cleandir(dir);
            recurseDeleteDir(dir);
            System.out.println("Create: " + dir);
            dir.mkdir();
            return true;
        }
    }

    private boolean recurseDeleteDir(File fdir) {
        boolean deleted;
        int here = 0;
        if (fdir.isDirectory()) { //если директория
            String[] children = fdir.list(); //Получаем список содержимого директории
            for (String child : children) {
                deleted = recurseDeleteDir(new File(fdir, child));
//                if (!deleted) {
//                    return false;
//                }
            }
        }
//        deleted = fdir.delete();
        System.out.println("Delete: " + fdir);
        return fdir.delete(); //deleted;
    }

    private String getSeparator() {
        String separator = System.getProperty("os.name"); //попробовать path.separator
        if (separator.startsWith("Windows")) {
            separator = "\\";
        } else {
            separator = "/";
        }
        return separator;
    }

    private void If(boolean startsWith) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
