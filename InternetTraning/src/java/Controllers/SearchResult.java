package Controllers;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement//(name = "Result")
@XmlType(propOrder = {"title", "link", "desc"})
@XmlAccessorType(XmlAccessType.NONE)

public class SearchResult {

    @XmlAttribute
    private String link;
    @XmlElement
    private String desc;
    @XmlAttribute
    private String title;
    private String name = "01";
    private Map<String, String> resultsMap = new HashMap();
    
    //конструктор
    public SearchResult() {

    }

    public SearchResult(String title, String desc, String link) {
        this.title = title;
        this.desc = desc;
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @XmlTransient //Анотация указывать не использовать поле
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getResultsMap() {
        return resultsMap;
    }

    public void setResultsMap(Map<String, String> resultsMap) {
        this.resultsMap = resultsMap;
    }
    
    

}
