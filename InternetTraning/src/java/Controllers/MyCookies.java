package Controllers;

import java.net.HttpURLConnection;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyCookies {
    
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpURLConnection urlconnect;
    private String cookie;

    //конструктор
    public MyCookies(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public MyCookies() {
       
    }
    
    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
    
    //Получаем куки от клиента (браузера) распаковываем их и возвращаем строку.
    public String cookieToString() {
        String result = "";
        Cookie cookies[] = request.getCookies(); //получаем куки от клиента
        for (Cookie cookie : cookies) {
            result += cookie.getName() + "=" + cookie.getValue() + "; ";
        }
        if (result.length() > 2) {
            result = result.substring(0, result.length() - 2);
        }
        
        return result;
    }
    
    public void getCookieFromURL(HttpURLConnection urlconnect) {
        String hdr, cookieKey, cookieValue;
        cookie = "";
        for (int i = 1; (hdr = urlconnect.getHeaderFieldKey(i)) != null; i++) {
            String testvalue = urlconnect.getHeaderField(i);
            int test = 0;
            if (hdr.equals("Set-Cookie")) {
                String field = urlconnect.getHeaderField(i);
                int k = field.indexOf("=");
                int k2 = field.indexOf(";");
                cookieKey = field.substring(0, k).trim();
                cookieValue = field.substring(k + 1, k2).trim();
                field = cookieKey + "=" + cookieValue;
                cookie += "; " + field;
            }
        }
        if (cookie.length() > 2) {
            cookie = cookie.substring(2);
        }   
        int q = 0;
        
//        Map<String, List<String>> headers = urlconnect.getHeaderFields();
//                List<String> values = headers.get("Set-Cookie");
//                cookieValue = null;
//                for (Iterator iter = values.iterator(); iter.hasNext();) {
//                    String v = iter.next().toString();
//                    if (cookieValue == null) {
//                        cookieValue = v;
//                    } else {
//                        cookieValue = cookieValue + ";" + v;
//                    }
//                }
    }
    
}
