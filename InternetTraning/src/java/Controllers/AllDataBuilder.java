package Controllers;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "ResultDate")
public class AllDataBuilder {
    
    private ArrayList<SearchResult> listOfResults = new ArrayList<>();

    public ArrayList<SearchResult> getListOfResults() {
        return listOfResults;
    }

    public void setListOfResults(ArrayList<SearchResult> listOfResults) {
        this.listOfResults = listOfResults;
    }
}
