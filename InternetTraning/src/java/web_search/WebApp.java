package web_search;

import Controllers.AllDataBuilder;
import Controllers.WebSearch;
import Controllers.SearchResult;
import Controllers.MyCookies;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet(name = "WebRouting", urlPatterns = {"/WebRouting"})
@WebServlet(
        urlPatterns = "/WebRouting",
        initParams
        = {
            @WebInitParam(name = "inputText", value = "java программист разработчик требования резюме")// Поисквый запрос по которому будем парсить
        }
)
public class WebApp extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String myUrlPath = request.getRequestURL().toString();
        int x = myUrlPath.lastIndexOf("/");
        if (x > -1) {
            myUrlPath = myUrlPath.substring(0, x);
        }

        request.setCharacterEncoding("UTF-8");
        String inputText = getServletConfig().getInitParameter("inputText"); //получаем из анотации параметры поискового запроса // request.getParameter("inputText");

//        //Вариант для множественного выбора параметров поиска, фильтрация
//        Enumeration<String> enumeration = request.getParameterNames();
//        String body = "", name, value;
//        while (enumeration.hasMoreElements()) {
//            name = enumeration.nextElement();
//            value = request.getParameter(name);
////          String[] values = request.getParameterValues(name);//Можно сделать через Map getParameterMap()
//            body += name + "=" + value + "&";
//            }
//        if (body.length()> -1){
//        body = body.substring(0,body.length()-1);
//        }
        SearchResult searchResult;

        //Получаем куки от клиента (браузера) в виде строки
        MyCookies mycookie = new MyCookies(request, response);
        String allCookiesString = mycookie.cookieToString();

        WebSearch сwebsearch = new WebSearch();
        сwebsearch.setCookie(allCookiesString);
        сwebsearch.setPath(getPathToDir());
        сwebsearch.setQuery(inputText);
//        ArrayList<SearchResult> outdata = сwebsearch.getInputText(inputText); // = websearch.readURL("https://uk.sports.yahoo.com/esp/tennis/");
        AllDataBuilder outdata = сwebsearch.getInputText(); //inputText// = websearch.readURL("https://uk.sports.yahoo.com/esp/tennis/");
        allCookiesString = сwebsearch.getCookie();
        String cookieTemp[] = allCookiesString.split("; ");

        for (String t : cookieTemp) {
            String te[] = t.split("=");
            int q = 0;
            if (te.length > 1) {
                Cookie myCookie = new Cookie(te[0], te[1]);
                myCookie.setMaxAge(3600 * 24 * 7);
                response.addCookie(myCookie);
            }
            q = 0;
        }

        request.setAttribute("outdata", outdata);
        request.setAttribute("query", inputText);
        RequestDispatcher rd = request.getRequestDispatcher("/output.jsp");
        rd.forward(request, response);

//Получить куки от майл ру, записать его в Куки, от дать его майл ру 
//        Cookie myCookie = new Cookie("cid", "qwerty");
//        myCookie.setMaxAge(3600*24*7);
//        response.addCookie(myCookie);
//        Iterator<SearchResult> iter = outdata.iterator();
//
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet WebRouting</title>");
//            out.println("</head>");
//            out.println("<body>");
//            while (iter.hasNext()) {
//                searchResult = iter.next();
//                out.println("<span><b>Title:</b> " + searchResult.getTitle() + "</span><br>");
//                out.println("<span><b>Description:</b> " + searchResult.getDesc() + "</span><br>");
//                out.println("<span><b>URL:</b> <a href=\"" + searchResult.getLink() + "\">Link</a></span>");
//                out.println("<span><b>Local URL:</b> <a target=\"_blank\" href=\"" + myUrlPath + "/ResultDir/" + searchResult.getTitle() + ".html" + "\">Link</a></span><hr>");
//            }
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    protected String getPathToDir() {
        String rootPath = getServletContext().getRealPath(""); // получаем путь с сепаратором на конце!

        return rootPath;
    }

    protected void cleandir(File dir) {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (!file.isDirectory()) {
                    file.delete();
                }
            }
        }
    }

    private void visitAllFiles(File dir) {

        if (dir.isDirectory()) {
            String[] children = dir.list();

            for (String child : children) {
                visitAllFiles(new File(dir, child));
            }
        } else {
            readFile2(dir);
        }
    }

    private String readFile2(File file) {

//        File file = new File(where);
        String what = "";

        if (file.exists()) {
            FileInputStream fis = null;
            int size;
            try {
                fis = new FileInputStream(file);
                size = fis.available();
                byte[] bytes = new byte[size];
                fis.read(bytes);
                what = new String(bytes, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace(System.err);
            } finally {
                try {
                    if (fis != null) {
                        fis.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }

        return what;
    }

    private void readFile1(String path, String name) {

        FileReader fr;
        String s, text = "";
        try {
            fr = new FileReader(path + name);
            BufferedReader br = new BufferedReader(fr);
            while ((s = br.readLine()) != null) {
                System.out.println(s);
//            text += s;
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
//return text;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
