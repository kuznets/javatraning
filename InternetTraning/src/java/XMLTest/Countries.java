package XMLTest;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Countries")
public class Countries {

    private List<Country> countries;

    @XmlElement(name = "Country")
    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public void add(Country country) {
        if (this.getCountries() == null) {
            this.setCountries(new ArrayList<Country>());
        }
        this.getCountries().add(country);

    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        for (Country museum : this.getCountries()) {
            str.append(museum.toString());
        }
        return str.toString();
    }

}