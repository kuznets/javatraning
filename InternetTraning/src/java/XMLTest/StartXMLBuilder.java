package XMLTest;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.TransformerException;

public class StartXMLBuilder {


    public static void main(String[] args) {
        try {
            Country spain = new Country();
            spain.setName( "Spain" );
            spain.setCapital( "Madrid" );
            spain.setContinent( "Europe" );
            spain.setPopulation(40000000);
            
            City city = new City();
            city.setTown("Madrid");
            city.setPeople(5000000);
            
            City city2 = new City();
            city2.setTown("Valencia");
            city2.setPeople(1000000);
            
            spain.getCities().add(city);
            spain.getCities().add(city2);
            
            Country usa = new Country();
            usa.setName( "USA" );
            usa.setCapital( "Washington" );
            usa.setContinent( "America" );
            usa.setPopulation(400000000);
            
            Countries countries = new Countries();
            countries.add( spain );
            countries.add( usa );
            
            BuildXML bx = new BuildXML();
            
            bx.buildXML(countries);
        } catch (TransformerException ex) {
            Logger.getLogger(StartXMLBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    
}
