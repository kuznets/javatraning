package XMLTest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import java.io.File;
import java.io.StringWriter;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class BuildXML {

    public void buildXML(Countries countries) throws TransformerConfigurationException, TransformerException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = docFactory.newDocumentBuilder();

            Document doc = builder.newDocument();
            Element rootElement = doc.createElement("Countries");
            doc.appendChild(rootElement);

            List<Country> listofcountries = countries.getCountries();
            for (Country country : listofcountries) {

                String name = country.getName();
                String capital = country.getCapital();
                String continent = country.getContinent();
                int population = country.getPopulation();

                Element e_country = doc.createElement("Country");
                rootElement.appendChild(e_country);

                Element e_name = doc.createElement("Country_Name");
                e_name.setTextContent(name);
                e_country.appendChild(e_name);

                Element e_capital = doc.createElement("Country_Capital");
                e_capital.setTextContent(capital);
                e_country.appendChild(e_capital);

                Element e_continent = doc.createElement("Country_Continent");
                e_continent.setTextContent(continent);
                e_country.appendChild(e_continent);

                Element e_population = doc.createElement("Country_Population");
                e_population.setTextContent(Integer.toString(population));
                e_country.appendChild(e_population);

                List<City> cities = country.getCities();
                for (City c : cities) {
                    Element e_city = doc.createElement("Country_City");
                    e_country.appendChild(e_city);

                    String town = c.getTown();
                    Element e_town = doc.createElement("town");
                    e_town.setTextContent(town);
                    e_city.appendChild(e_town);

                    int people = c.getPeople();
                    Element e_people = doc.createElement("people");
                    e_people.setTextContent(Integer.toString(people));
                    e_city.appendChild(e_people);

                }

            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("test_countries.xml"));

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);
            StreamResult xmlOutput = new StreamResult(new StringWriter());
            transformer.transform(source, xmlOutput);

            System.out.println(xmlOutput.getWriter().toString());

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(BuildXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    String writeXML(int tabcount, String repName, ArrayList documentsList) throws UnsupportedEncodingException {

        StreamResult xmlOutput = new StreamResult(new StringWriter());

//        try {
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//		DocumentBuilder builder = docFactory.newDocumentBuilder();
        // root elements
//		Document doc = builder.newDocument();
//		Element rootElement = doc.createElement("Report");
//                rootElement.setAttribute("repID", Integer.toString(tabcount));
//		doc.appendChild(rootElement);
//            for (DocumentReport documentReport : documentsList) {
//ArrayList<Integer> snipIDList = documentReport.getSnipID();// Integer.toString()
//    ArrayList<String> rankList = documentReport.getRank();//
//    ArrayList<String> textList = documentReport.getText();//
//    int i, n = snipIDList.size();
//    if(n>0){
//               Element document = doc.createElement("document");
//		rootElement.appendChild(document);
//		Attr docID = doc.createAttribute("docID");
//		docID.setValue(Integer.toString(documentReport.getDocID()));
//		document.setAttributeNode(docID);
//                
//                Attr sumRank = doc.createAttribute("sumRank");
//		sumRank.setValue(documentReport.getSumRank());
//		document.setAttributeNode(sumRank);
//                Element title = doc.createElement("title");
//                title.setTextContent(documentReport.getTitle());
//		document.appendChild(title);
//                Element location = doc.createElement("location");
//                Attr type = doc.createAttribute("type");
//		type.setValue("URL");
//		location.setAttributeNode(type);
//                location.setTextContent(documentReport.getUrladdr());
//		document.appendChild(location);
//    Element snippets = doc.createElement("snippets");
//                snippets.setAttribute("size", Integer.toString(n));
//                document.appendChild(snippets);
//    for(i=0;i<n;i++){
//        Element aSnippet = doc.createElement("aSnippet");
//        Attr snipID = doc.createAttribute("snipID");
//		snipID.setValue(Integer.toString(snipIDList.get(i)));
//                aSnippet.setAttributeNode(snipID);
//                
//                Attr rank = doc.createAttribute("rank");
//		rank.setValue(rankList.get(i));
//                aSnippet.setAttributeNode(rank);
//                
//                aSnippet.setTextContent(textList.get(i));
//        snippets.appendChild(aSnippet);
//    }
//    }
//  }
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		Transformer transformer = transformerFactory.newTransformer();
//		DOMSource source = new DOMSource(doc);
//		StreamResult result = new StreamResult(new File(repName));

//                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
//		transformer.transform(source, result);
//                transformer.transform(source, xmlOutput);
//if(!eMailAddress.equals("")){
//String message = "REPORT";
//String MsgSubject = "REPORT";
//
//postMail(eMailAddress, message, MsgSubject, repName);
//}
//} catch (ParserConfigurationException | FactoryConfigurationError | TransformerException ex) {
//            Logger.getLogger(BuildXML.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return xmlOutput.getWriter().toString();
        return "";
    }

}
