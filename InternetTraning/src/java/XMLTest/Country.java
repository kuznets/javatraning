package XMLTest;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name", "capital", "continent", "population", "cities"})
@XmlRootElement//(name = "Country")
public class Country {

    private String name;
    private String capital;
    private String continent;
    private int population;
    private List<City> cities = new ArrayList();

    
    public List<City> getCities() {
        return cities;
    }

    @XmlElement(name = "Country_City")
    public void setCities(List<City> cities) {
        this.cities = cities;
    }
    
    public int getPopulation() {
        return population;
    }

    @XmlElement(name = "Country_Population")
    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Country_Name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer("Name: " + getName() + "\n");
        str.append("Capital: " + getCapital() + "\n");

        if (getContinent() != null) {
            str.append(getContinent());
            str.append("\n");
        }

        return str.toString();
    }

    public String getCapital() {
        return capital;
    }

    @XmlElement(name = "Country_Capital")
    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getContinent() {
        return continent;
    }

    @XmlElement(name = "Country_Continent")
    public void setContinent(String continent) {
        this.continent = continent;
    }
}
