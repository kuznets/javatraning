package websockets4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import javax.websocket.EncodeException;
import javax.websocket.Session;

public class TimerSubmit implements Runnable {

    private boolean firstrun = true;
    private boolean running = false;
    private int minute = -1;
    private int second = -1;
    private int nextrun = -1;

    private ActionListener listener;
    private Timer timer;
    private final Session wssession;

    private final ArrayList<String> tasksList;
    private int currentIndex = 0;

    public TimerSubmit(Session wssession, Message message) {
        this.wssession = wssession;

        tasksList = new ArrayList<>();
        tasksList.add("Task #1");
        tasksList.add("Task #2");
    }

    @Override
    public void run() {

//         listener = new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
        listener = (ActionEvent e) -> {
            Calendar calendar = Calendar.getInstance();
            minute = calendar.get(Calendar.MINUTE);
            second = calendar.get(Calendar.SECOND);

            doJob();
        };

        timer = new Timer(5000, listener);
        timer.start();
    }

    private void doJob() {
        int test = 0;
        if (firstrun || minute == nextrun) {

            firstrun = false;
            nextrun = minute + 2;
            if (nextrun > 60) {
                nextrun -= 60;
            }
            if (tasksList.size() > currentIndex) {
                String task = tasksList.get(currentIndex);
                currentIndex++;
                pushMessage("Processing task #" + (currentIndex), task);

            } else if (tasksList.size() == currentIndex) {
                nextrun = -1;
                minute = -1;
                firstrun = true;
                timer.stop();
                currentIndex = 0;
                pushMessage("All submitted tasks completed!", "");
            }
        } else if (nextrun > minute) {
            pushMessage(minute + ":" + second, (nextrun - minute) + " min left.");
        }

    }

    private void pushMessage(String text1, String text2) {

//        JsonObject jsonObject = Json.createObjectBuilder()
//				.add("text1", text1)
//				.add("text2", text2)                        
//                                .build();
        Message message = new Message();
        message.setSubject(text1);
        message.setBody(text2);

        if (wssession.isOpen()) {
            try {
                //            wssession.getBasicRemote().sendObject(jsonObject.toString());
                wssession.getBasicRemote().sendObject(message);
            } catch (IOException | EncodeException ex) {
                Logger.getLogger(TimerSubmit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
