package websockets4;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class MessageDecoder implements Decoder.Text<Message> {

    @Override
    public Message decode(String jsonMessage) throws DecodeException {

        Message message = null;
        System.out.println("Incoming JSON " + jsonMessage);

//        JsonReader reader = Json.createReader(new StringReader(jsonMessage));
//        JsonArray jsonArray = reader.readArray();
//        JsonReader reader = Json.createReader(new StringReader(jsonMessage));
//        JsonObject jsonObject = reader.readObject();
//        reader.close();
        JsonObject jsonObject = Json
                .createReader(new StringReader(jsonMessage)).readObject();
        message = new Message();
        message.setSubject(jsonObject.getString("subject"));
        message.setBody(jsonObject.getString("body"));

//        Gson gson = new Gson();
//        Type typeOf = new TypeToken<List<SearchRequest>>() {
//        }.getType();
//        try {
//            message = new Message();
//             message.setList((List<SearchRequest>)gson.fromJson(jsonMessage, typeOf));
//        } catch (JsonSyntaxException | ClassCastException e) {
//            e.printStackTrace(System.err);
//        }
//        Message message = gson.fromJson(jsonMessage, Message.class);
        return message;

    }

    @Override
    public boolean willDecode(String jsonMessage) {
        try {
            Json.createReader(new StringReader(jsonMessage)).readObject();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void init(EndpointConfig ec) {
        System.out.println("MessageDecoder -init method called");
    }

    @Override
    public void destroy() {
        System.out.println("MessageDecoder - destroy method called");
    }

}
