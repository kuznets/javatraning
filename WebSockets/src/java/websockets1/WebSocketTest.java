package websockets1;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket1")
public class WebSocketTest {

    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException, InterruptedException {

                session.getBasicRemote().sendText("Received: " + message);

        Thread.sleep(1500);
        session.getBasicRemote().sendText("This is the first server message");

        int sentMessages = 1;
        while (sentMessages < 6) {
            Thread.sleep(1500);
            session.getBasicRemote().
                    sendText("Message # "
                            + sentMessages);
            sentMessages++;
        }
        Thread.sleep(1500);
        session.getBasicRemote().sendText("This is the last server message");
    }

    @OnOpen
    public void onOpen() {
        System.out.println("Client connected");
    }

    @OnClose
    public void onClose() {
        System.out.println("Connection closed");
    }

    @OnError
    public void onError(Throwable t) {
        System.out.println("Connection Error");
    }
}
