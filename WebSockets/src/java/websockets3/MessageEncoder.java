package websockets3;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class MessageEncoder implements Encoder.Text<Message> {

    @Override
    public String encode(Message message) throws EncodeException {

        JsonObject jsonObject = Json.createObjectBuilder()
                .add("name", message.getName())
                .add("subject", message.getSubject())
                .add("body", message.getBody())
                .build();
        
        return jsonObject.toString();
    }

    @Override
    public void init(EndpointConfig ec) {
        System.out.println("MessageEncoder - init method called");
    }

    @Override
    public void destroy() {
        System.out.println("MessageEncoder - destroy method called");
    }

}
