package websockets3;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class MessageDecoder implements Decoder.Text<Message> {

    @Override
    public Message decode(String jsonMessage) throws DecodeException{

        JsonObject jsonObject = Json
                .createReader(new StringReader(jsonMessage)).readObject();
        Message message = new Message();
        message.setName(jsonObject.getString("name"));
        message.setSubject(jsonObject.getString("subject"));
        message.setBody(jsonObject.getString("body"));

        return message;
    }

    @Override
    public boolean willDecode(String jsonMessage) {
        try {
            Json.createReader(new StringReader(jsonMessage)).readObject();
            return true;
        } catch (Throwable t) {
            t.printStackTrace(System.err);
            return false;
        }
    }

    @Override
    public void init(EndpointConfig ec) {
        System.out.println("MessageDecoder -init method called");
    }

    @Override
    public void destroy() {
        System.out.println("MessageDecoder - destroy method called");
    }

}
