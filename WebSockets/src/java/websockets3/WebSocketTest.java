package websockets3;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/websocket3", encoders = {MessageEncoder.class}, decoders = {MessageDecoder.class})
public class WebSocketTest {

    @OnMessage
    public void onMessage(Message message, Session session) throws IOException,
            EncodeException {

        Message response = new Message();
        response.setSubject(message.getSubject());
        response.setBody(message.getBody());
        response.setName(message.getName());

        session.getBasicRemote().sendObject(response);
    }

    @OnOpen
    public void onOpen() {
        System.out.println("Client connected");
    }

    @OnClose
    public void onClose() {
        System.out.println("Connection closed");
    }

    @OnError
    public void onError(Throwable e, Session session) {
        System.out.println("Error");
        e.printStackTrace(System.err);
    }

}
