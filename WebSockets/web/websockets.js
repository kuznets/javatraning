angular.module('wsApp', [])

        .controller('wsController', function ($scope, echoSocket) {

            echoSocket.onmessage = function (event) {

//                $scope.response = event.data;// 1,2 (with 2 - use different tabs)

                var messageObj = JSON.parse(event.data);// 3,4

//                $scope.lb4 = messageObj.name + ' - ' + messageObj.subject;// 3

                $scope.lb4 = messageObj.subject;// 4
//
                $scope.lb4a = messageObj.body;// 3,4

                $scope.$apply();
            };

            $scope.message = {name: 'MyName', subject: 'MySubject', body: 'MyBody'};

            $scope.postData = function () {

//                                echoSocket.send('QWERTy');// 1,2

                echoSocket.send(JSON.stringify($scope.message));// 3,4
//
                $scope.lb4 = "Processing started.";// 3,4
                $scope.lb4a = "Please wait...";// 3,4
            };

        })

        .factory('echoSocket', function () {
            var wsUri;
            if (window.location.hostname === 'localhost') {
                wsUri = 'ws://localhost:8080/WebSockets/websocket4';//1 2 3 4
            }

            return new WebSocket(wsUri);
        })

        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter); //$eval применить
                        });

                        event.preventDefault();
                    }
                });
            };
        });
//http://www.w3schools.com/jsref/jsref_eval.asp