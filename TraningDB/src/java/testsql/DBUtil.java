package testsql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBUtil {

    private Statement statement = null;
    private ResultSet resultSet = null;
    private Connection connection = null;
    private dbPool dbPool = null;

    private String driverClass = "org.gjt.mm.mysql.Driver";
    private String dbUrl = "jdbc:mysql://localhost:3306/traning_parser";
    private String username = "root";
    private String password = "";

    public void initialize() {
//        connection = getConnection();

        if (dbPool == null) {
            try {
                dbPool = new dbPool(driverClass, dbUrl, username, password);
            } catch (SQLException e) {
                e.printStackTrace(System.err);
            }
//            dbPool = createConnectionPool();
        }
        if (dbPool != null) {
            try {
                connection = dbPool.getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (connection == null) {
            System.out.println("db connection failure (1)");
        } else {
            System.out.println("db connection has been established");
        }
    }

//    private Connection getConnection() {
//        String driverClass = "org.gjt.mm.mysql.Driver";
//        String dbUrl = "jdbc:mysql://localhost:3306/traning_parser";
//        String username = "root";
//        String password = "";
//        try {
////////            Class c = Class.forName("org.gjt.mm.mysql.Driver");
////////            Driver driver = (Driver) c.newInstance();
////////            DriverManager.registerDriver(driver);
//            Class.forName(driverClass); //активация драйвера
//            connection = DriverManager.getConnection(dbUrl, username, password);
//            return (connection);
//        } catch (ClassNotFoundException | SQLException sqle) {
//            System.err.println("Error connecting: " + sqle);
//            return (null);
//        }
//    }
//    private dbPool createConnectionPool() {
//        String driverClass = "org.gjt.mm.mysql.Driver", dbUrl = "jdbc:mysql://localhost:3306/traning_parser",
//                username = "root", password = "";
//
//        try {
//            dbPool = new dbPool(driverClass, dbUrl, username, password);
//        } catch (SQLException e) {
//            e.printStackTrace(System.err);
//        }
//        return dbPool;
//    }
    public void cleanUp() {

        try {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (dbPool != null && connection != null) {
                dbPool.release(connection);//release - открытое (соед)
            }
            if (dbPool == null && connection != null) {
                connection.close();
            }

//            System.gc();
        } catch (SQLException e) {
            e.printStackTrace(System.err);

        }

    }

    public void createTable(String table_name) {

        try {
            statement = connection.createStatement(); //Инструмент для передачи sql комманды в БД
        } catch (SQLException e) {
        }

        try {

            String create_table = "CREATE TABLE " + table_name;
            if (table_name.equals("users")) {

                create_table
                        += " (user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, "
                        + "first_name VARCHAR(40) NOT NULL, "
                        + "last_name VARCHAR(40) NOT NULL, "
                        + "user_email VARCHAR(30) NOT NULL UNIQUE KEY, "
                        + "user_pwd VARCHAR(10) NOT NULL"
                        + ") CHARACTER SET utf8";

                statement.execute(create_table);

            }

        } catch (SQLException sqle) {
            System.err.println("Error creating table (2): " + sqle);
        }

    }

    public void dropTable(String table_name) {

        try {
            statement = connection.createStatement();
            statement.execute("DROP TABLE " + table_name);
        } catch (SQLException sqle) {
        }

    }

    public int addRow_users(String first_name, String last_name,
            String user_email, String user_pwd) {

        int user_id = 0;

        String insert_row = "INSERT INTO users";

        insert_row
                += " (" + "first_name, "
                + "last_name, "
                + "user_email, "
                + "user_pwd"
                + ")"
                + " VALUES("
                + "'" + first_name + "', "
                + "'" + last_name + "', "
                + "'" + user_email + "', "
                + "'" + user_pwd
                + "')";

        try {

            statement = connection.createStatement();
            statement.execute(insert_row);

            resultSet = statement.executeQuery("SELECT LAST_INSERT_ID()");

            if (resultSet.next()) {
                user_id = resultSet.getInt(1);
            }

        } catch (SQLException sqle) {
            System.out.println("update failure");
            System.err.println(sqle);
        }

        return user_id;
    }

    public void removeRow(int user_id, String tableName) {

        String deleteRow = "DELETE FROM " + tableName + " WHERE USER_ID = '" + user_id + "'";

        try {
            statement = connection.createStatement();

            statement.execute(deleteRow);

        } catch (SQLException sqle) {

        }
    }

    public int addRow_users_prepst(String first_name, String last_name,
            String user_email, String user_pwd) {

        int user_id = 0;

        String insert_row = "INSERT INTO users";
        insert_row
                += " (" + "first_name, "
                + "last_name, "
                + "user_email, "
                + "user_pwd"
                + ")"
                + " VALUES(?,?,?,?)";

        try {
            PreparedStatement pstmt = connection.prepareStatement(insert_row);

            pstmt.setString(1, first_name);
            pstmt.setString(2, last_name);
            pstmt.setString(3, user_email);
            pstmt.setString(4, user_pwd);

            pstmt.executeUpdate();

            resultSet = pstmt.executeQuery("SELECT LAST_INSERT_ID()");

            if (resultSet.next()) {
                user_id = resultSet.getInt(1);
            }

        } catch (SQLException sqle) {
            System.out.println("update failure");
            System.err.println(sqle);
        }

        return user_id;
    }

    public String updateRow(String user_id, String PASSWORD,
            String FIRST_NAME,
            String LAST_NAME, String E_MAIL,
            String ORG, String AGE) {

        String update_users = "", update_passwords = "", remaining = "", getremaining = "";

        try {
            statement = connection.createStatement();

//String getUSER_ID = "SELECT USER_ID FROM passwords WHERE E_MAIL= " + E_MAIL;
//        ResultSet resultSet = statement.executeQuery(getUSER_ID);
//        resultSet.next();
//        int USERID = resultSet.getInt("USER_ID");
//        String insert_passwords = "INSERT INTO passwords " +
//              "(E_MAIL, PASSWORD, remaining) " +
//              "VALUES(" + E_MAIL + ", " + PASSWORD + ", " + "3)";
            statement.execute(update_passwords);

            update_users
                    = "UPDATE users SET"
                    + " FIRST_NAME = '" + FIRST_NAME + "', LAST_NAME='" + LAST_NAME
                    + "' WHERE USER_ID = '" + user_id + "'";

//  String insert_users = "INSERT INTO users " +
//          "(FIRST_NAME, LAST_NAME, " +
//           "ORG, AGE) " +
//          
//          "VALUES(" + FIRST_NAME + ", " + LAST_NAME + ", " + 
//           ORG + ", " + AGE + ")";
//  
            statement.execute(update_users);

            getremaining = "SELECT PASSWORD FROM users WHERE USER_ID = '" + user_id + "'";
//        int USERID = 0;      
            resultSet = statement.executeQuery(getremaining);
            resultSet.next();
            remaining = Integer.toString(resultSet.getInt("remaining"));

        } catch (SQLException sqle) {
            System.out.println("update failure");
            System.err.println(sqle);
        }

        return remaining;
    }

    public String getPass(String user_email) {

        String getPass = "SELECT user_pwd FROM USERS WHERE user_email = '" + user_email + "'"; //SELECT * Все или через запятую поля

        try {

            statement = connection.createStatement();

            resultSet = statement.executeQuery(getPass);

            if (resultSet.next()) { //while если надо несколько результатов
                return resultSet.getString("user_pwd");

            } else {
                return "Email not found!";
            }

//             while (resultSet.next()) {
//
//                    if (table_name.equals("client_tab1")) {
//
//                        v.add("<br><br>client_id: " + resultSet.getInt("client_id"));
//                        v.add("client_name: " + resultSet.getString("client_name"));
//                        v.add("client_inn: " + resultSet.getString("client_inn"));
//                        v.add("client_kpp: " + resultSet.getString("client_kpp"));
//                        v.add("client_address: " + resultSet.getString("client_address"));
//                        v.add("contact1_name: " + resultSet.getString("contact1_name"));
//                        v.add("contact1_aff: " + resultSet.getString("contact1_aff"));
//                        v.add("contact1_phone: " + resultSet.getString("contact1_phone"));
//                        v.add("contact2_name: " + resultSet.getString("contact2_name"));
//                        v.add("contact2_aff: " + resultSet.getString("contact2_aff"));
//                        v.add("contact2_phone: " + resultSet.getString("contact2_phone"));
//                        v.add("fin_info_bank: " + resultSet.getString("fin_info_bank"));
//                        v.add("fin_info_bik: " + resultSet.getString("fin_info_bik"));
//                        v.add("fin_info_corr: " + resultSet.getString("fin_info_corr"));
//                        v.add("fin_info_acc: " + resultSet.getString("fin_info_acc"));
//                        v.add("client_login: " + resultSet.getString("client_login"));
//                        v.add("client_pwd: " + resultSet.getString("client_pwd"));
//                        v.add("client_regdate: " + formatter.format(resultSet.getDate("client_regdate")));
//                        v.add("technologist_id: " + resultSet.getInt("technologist_id"));
//                        v.add("technologist_name: " + resultSet.getString("technologist_name"));
//                        v.add("manager_id: " + resultSet.getInt("manager_id"));
//                        v.add("manager_name: " + resultSet.getString("manager_name"));
//
//                    }
        } catch (SQLException sqle) {

        }

        return "System error";

    }

    void test() {
        try {
            statement.execute("CREATE TABLE employee ( "
                    + "	id smallint(5) unsigned NOT NULL, "
                    + "	firstname varchar(30), "
                    + "	lastname varchar(30), "
                    + "	birthdate date, "
                    + "	PRIMARY KEY (id), "
                    + "	KEY idx_lastname (lastname) "
                    + ") ENGINE=InnoDB");
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement.execute("CREATE TABLE borrowed ( "
                    + "	ref int(10) unsigned NOT NULL auto_increment, "
                    + "	employeeid smallint(5) unsigned NOT NULL, "
                    + "	book varchar(50), "
                    + "	PRIMARY KEY (ref) "
                    + ") ENGINE=InnoDB");
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement.execute("ALTER TABLE borrowed "
                    + "ADD CONSTRAINT FK_borrowed "
                    + "FOREIGN KEY (employeeid) REFERENCES employee(id) "
                    + "ON UPDATE CASCADE "
                    + "ON DELETE CASCADE"); //удалит/отредактирует запись в дочерних таблицах
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            statement.execute("SELECT book FROM borrowed \n"
                    + "JOIN employee ON employee.id=borrowed.employeeid \n"
                    + "WHERE employee.lastname='Smith'");
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement.execute("UPDATE employee SET id=22 WHERE id=2");
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            statement.execute("DELETE FROM employee WHERE id=1");
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
