package testsql;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DataBaseServlet", urlPatterns = {"/DataBaseServlet"})
public class DataBaseServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("first_name");
        String surname = request.getParameter("last_name");
        String email = request.getParameter("user_email");
        String password = request.getParameter("user_pwd");
        int task = Integer.parseInt(request.getParameter("task"));

        String output = "ok";
        DBUtil dbutil = new DBUtil();
        dbutil.initialize();

        switch (task) {
            case 1:
                dbutil.createTable("users");
                break;
            case 2:
                dbutil.dropTable("users");
                break;
            case 3:
                output =Integer.toString(dbutil.addRow_users_prepst(name, surname, email, password));
                break;
                case 4:
                dbutil.removeRow(3, "users");
                break;
                case 5:
                output = dbutil.getPass(email);
                break;
            default:
                output = "Ошибка ввода";
                break;
        }

        dbutil.cleanUp();

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DataBaseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(output);
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
