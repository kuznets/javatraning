<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>My Cabinet</h2>
        <ul class="menu">
            <li><a href="index.jsp">Main Page</a></li>
            <li><a href="REList">Real Estate List</a></li>
        </ul>
        <%
            String listindex = "";
            if (request.getAttribute("listindex") != null) {
                listindex = request.getAttribute("listindex").toString();
            }
        %>
        <form action="Cabinet">
            <select name="list" onchange="this.parentElement.submit()">
                <option>Select type of real estate:</option>
                <option <%
                    if (listindex.equals("Apartments")) {
                        out.println("selected");
                    }
                    %> value="Apartments">Apartments</option>
                <option <%
                    if (listindex.equals("House")) {
                        out.println("selected");
                    }
                    %> value="House">House</option>
                <option <%
                    if (listindex.equals("HousePart")) {
                        out.println("selected");
                    }
                    %> value="HousePart">House Part</option>
                <option <%
                    if (listindex.equals("Commerce")) {
                        out.println("selected");
                    }
                    %> value="Commerce">Commerce</option>
                <option <%
                    if (listindex.equals("Land")) {
                        out.println("selected");
                    }
                    %> value="Land">Land</option>
                <option <%
                    if (listindex.equals("SummerCottage")) {
                        out.println("selected");
                    }
                    %> value="SummerCottage">Summer Cottage</option>
                <option <%
                    if (listindex.equals("Garage")) {
                        out.println("selected");
                    }
                    %> value="Garage">Garage</option>
            </select>
        </form>
    </body>
</html>
