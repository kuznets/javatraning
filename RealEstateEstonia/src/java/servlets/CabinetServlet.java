/*
Author: Sergei Reshetnjak
e-mail: sresetnjak@gmail.com
********************************
Cabinet servlet
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import config.config;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/Cabinet")

public class CabinetServlet extends HttpServlet {

    private String readURL(String index){
        String urlAddress = getURL(index);
        StringBuilder sb = new StringBuilder();
        
        try {
            URL url = new URL(urlAddress);

            InputStream is = url.openStream(); //открываем входной поток
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is))) { //читаем текст в буфер из потока / декодируем байты в символы в заданной кодировке
                String line = "";
                String newLine = System.lineSeparator();//Перевод каретки для нашей ОС
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                    sb.append(newLine);
                }
            } catch (IOException ex) {
                Logger.getLogger(CabinetServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(CabinetServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CabinetServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        String urllist = parseURLList(sb.toString());

        return urllist;
    }
    
    private String parseURLList(String src){
        String out = "", item;
        src = src.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "")
                .replaceAll(" +", " ");//чистим текст
        int x = src.indexOf("id=\"list-container\">");//начало контента
        int y = src.indexOf("id=\"endOfListElements\">");//конец контента
        String split = "<li class=\"new result regular\">";// разделитель на куски
        String s[] = src.substring(x, y).split(split); //делим на куски
        for (int i = 1; i < s.length; i++) {
            item = s[i];
            String tmp = "";
            
            int x1 = item.indexOf("address-wrap", 0);//находим индекс откуда будем искать ссылку
            x1 = item.indexOf("<a href=\"", x1);//находим индекс начала ссылки
            int x2 = item.indexOf("\" class=\"addressLink\"", x1);//находим индекс конца ссылки
            if (x1 > -1 && x2 > x1) {
                tmp += item.substring(x1 + 9, x2) + " | ";
            }
            out = "";
        }
        out = "";
        return out;
    }
    
    private String getURL(String index) {
        config config = new config();
        String url = "Not found";
        switch (index) {
            case "Apartments":
                url = config.getParseApartmentsURL();
                break;
            case "House":
                url = config.getParseHouseURL();
                break;
            case "HousePart":
                url = config.getParseHousePartURL();
                break;
            case "Commerce":
                url = config.getParseCommerceURL();
                break;
            case "Land":
                url = config.getParseLandURL();
                break;
            case "SummerCottage":
                url = config.getParseSummerCottageURL();
                break;
            case "Garage":
                url = config.getParseGarageURL();
                break;
        }
        return url;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String index = request.getParameter("list");
        //For *.jsp page
        request.setAttribute("listindex", index);
        request.getRequestDispatcher("/cabinet.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
