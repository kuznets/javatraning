/*
Author: Sergei Reshetnjak
e-mail: sresetnjak@gmail.com
********************************
Config file.
*/
package config;

public class config {
    //URL Adreses for parse
    private String parseApartmentsURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/kvartira/estonia?ord=1&str=2&usp=true&c=EE&fr=0&lang=ru";
    //private String parseApartments =  "http://www.city24.ee/ru/spisok/prodazha/arendovat/kvartira/estonia?ord=1&str=2&usp=true&c=EE&fr=1&lang=ru";
    //                                   http://www.city24.ee/ru/spisok/prodazha/arendovat/kvartira/estonia?ord=1&str=2&usp=true&c=EE&fr=2&lang=ru
    private String parseHouseURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/dom/estonia?ord=1&str=2&usp=true&c=EE&fr=0&lang=ru";
    private String parseHousePartURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/chastdoma/estonia?ord=1&str=2&usp=true&c=EE&";
    private String parseCommerceURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/kommercheskij/estonia?ord=1&str=2&usp=true&c=EE&";
    private String parseLandURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/uchastok+zemli/estonia?ord=1&str=2&usp=true&c=EE&";
    private String parseSummerCottageURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/dacha/estonia?ord=1&str=2&usp=true&c=EE&";
    private String parseGarageURL = "http://www.city24.ee/ru/spisok/prodazha/arendovat/garaza/estonia?ord=1&str=2&usp=true&c=EE&";

    public String getParseApartmentsURL() {
        return parseApartmentsURL;
    }

    public String getParseHouseURL() {
        return parseHouseURL;
    }

    public String getParseHousePartURL() {
        return parseHousePartURL;
    }

    public String getParseCommerceURL() {
        return parseCommerceURL;
    }

    public String getParseLandURL() {
        return parseLandURL;
    }

    public String getParseSummerCottageURL() {
        return parseSummerCottageURL;
    }

    public String getParseGarageURL() {
        return parseGarageURL;
    }
    
}
