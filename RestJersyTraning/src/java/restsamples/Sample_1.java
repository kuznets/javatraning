package restsamples;

import domains.Welcome;
import java.io.File;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Path("rest")
//@Path("/MyPath_1/{username}") 
//@Path("")
//@Singleton
public class Sample_1 {
    int coumt = 0;

    // http://localhost:8084/WebApplication1/web/rest/meping
    @GET
    @Path("mping{}")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
        return "test";
    }

    // http://localhost:8084/WebApplication1/webresources/thetest/get_xml
    @GET
    @Path("get_xml{}")
    @Produces(MediaType.APPLICATION_XML)
    public Welcome getXml() {
        Welcome wl = new Welcome();
        wl.setDomain("www");
        wl.setMessage("aaa");
        return wl;
    }

    //   http://localhost:8084/WebApplication1/webresources/thetest/getjson2
    @GET
    @Path("/getjson2")
    @Produces(MediaType.APPLICATION_JSON)
    public Welcome simplejson() {
        Welcome welcome = new Welcome();
        welcome.setDomain("www.yandex.ru");
        welcome.setMessage("JSON Example");

        return welcome;
    }

    //    http://localhost:8080/JREST/webresources/MyPath_3/qwerty
    @Path("/MyPath_3/{username}")
    @GET
    @Produces("text/plain")
    public String getText(@PathParam("username") String name) {
        return "UserName: " + name;
    }

    //    http://localhost:8080/JREST/webresources/MyPath2/age/11
    @GET
    @Path("/age/{j}")
    @Produces(MediaType.TEXT_XML)
    public String userAge(@PathParam("j") int age) {
        return "<User>" + "<Age>" + age + "</Age>" + "</User>";
    }

    //http://localhost:8080/JREST/webresources/MyPath2/MyMethod?name=йцукен&age=3
    @Path("/MyMethod")
    @GET
    @Produces(MediaType.TEXT_HTML + "; charset=utf-8")
//    @Produces("text/html; charset=UTF-8")
    public String getQuery(@QueryParam("name") String name, @QueryParam("age") int age) {
        return "<h1>Query Parameters - " + "Name: " + name + ", Age: " + age + "</h1>";
    }

    //    http://localhost:8080/JRESTBasics/webresources/MyPath2/MyMethod22;name=qwe;age=11
    @Path("/MyMethod22")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getMatrix(@MatrixParam("name") String name, @MatrixParam("age") int age) {
        return "Matrix Parameters - " + "Name: " + name + ", Age: " + age;
    }

    //    http://localhost:8080/JRESTBasics/webresources/MyPath2/MyMethod2;name=йцукен;age=11
    @Path("/MyMethod3")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMatrix3(@MatrixParam("name") String name, @MatrixParam("age") int age) {
        return Response.ok("Name: " + name + ", Age: " + age).
                header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN + "; charset=utf-8").header(HttpHeaders.ACCEPT_CHARSET, "utf-8").build();
    }

    //    http://localhost:8080/JREST/webresources/MyPath2/getData
    @GET
    @Path("/getData")
    @Produces("image/jpg")
    public Response getData() {
        File f = new File("/Users/Apple/Desktop/cats/test.jpg");
        Response.ResponseBuilder builder = Response.ok((Object) f);
        builder.header("Content-Disposition", "attachment; filename=\"file_from_server.jpg\"");
        return builder.build();
    }

    @Path("/testFormInput")
    @POST
    @Produces("text/plain")
    public Response getData(@FormParam("user") String user) {
        return Response.ok(user).build();
    }
    
    @Path("/testFormInput2")
    @POST
    @Produces("text/plain")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String putName(@FormParam("user") String name) {
        return name;
    }
    
        //http://localhost:8080/JRESTBasics/webresources/MyPath2/Mydata
    @Path("/Mydata")
    @GET
    @Produces("application/json")
    public Response getJSONArray() {
        JSONObject jSONObject = new JSONObject();
        JSONArray array = new JSONArray();

        for (int i = 0; i < 5; i++) {
            jSONObject.put("id#" + i, i);
        }
        array.add(jSONObject);
        String h = array.toString();
        
        return Response.ok(h).build();
    }
    
    //use singleton строка 28
    @GET
    @Path("qwerty")
    @Produces(MediaType.TEXT_PLAIN)
    public int ping2() {
        coumt++;
        return coumt;
    }
    
}
