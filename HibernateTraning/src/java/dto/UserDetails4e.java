package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS4e1")
public class UserDetails4e implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;
    @ManyToMany
    private Collection<Vehicle4e> vehiclesList = new ArrayList(); //Лучше использовать List<Vehicle4b> ...
    
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<Vehicle4e> getVehiclesList() {
        return vehiclesList;
    }

    public void setVehiclesList(Collection<Vehicle4e> vehiclesList) {
        this.vehiclesList = vehiclesList;
    }
    
}
