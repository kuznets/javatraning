//Создаем таблицу с указанным нами именем (через анотации) создаем 3 столбца изпользуя анотации для их кастомизации и наполняем их
package dto;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity(name = "User_Details_1") // Задаем имя таблицы
public class UserDetails1 {
    //PrimaryKeyJoinColumn //Устанавливает уникальное значение

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)//авто генерация ИД, SEQUENCE - линейно
    //if use SEQUENCE => org.hibernate.MappingException: org.hibernate.dialect.MySQLDialect does not support sequences
    @Column(name = "USER_ID")// Задаем имя столбца
    private int userID;
    //@Id
    @Column(name = "USER_NAME")
    private String userName;
    @Temporal(TemporalType.DATE) //говорит о том, что будет использоваться для хранения даты
    private Date joinedDate;
    @Lob // говорит нам, что поле сущности является большим бинарным объектом (LOB)
    private String description;
    //@Basic(fetch = "") // анотация указывает писать эту колонку в БД
    @Transient //анотация указывает не писать эту колонку в БД
    private String address ;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(Date joinedDate) {
        this.joinedDate = joinedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
}
