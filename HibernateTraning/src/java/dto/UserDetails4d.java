package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS4d")
public class UserDetails4d implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;
    @OneToMany (mappedBy = "user")
    private Collection<Vehicle4d> vehiclesList = new ArrayList(); //Лучше использовать List<Vehicle4b> ...
    
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<Vehicle4d> getVehiclesList() {
        return vehiclesList;
    }

    public void setVehiclesList(Collection<Vehicle4d> vehiclesList) {
        this.vehiclesList = vehiclesList;
    }
    
}
