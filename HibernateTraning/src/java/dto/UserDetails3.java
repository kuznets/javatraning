//Создаем таблицу User_Details_3 к которой присоединяем таблицу USER_ADDRESS_3 (делаем связные таблицы по USER_ID (one to one)
package dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity(name = "User_Details_3")
public class UserDetails3 implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;
    @ElementCollection //указываем что это будет коллекция
    @JoinTable(name = "USER_ADDRESS_3", //даем кастомное имя таблице
            joinColumns = @JoinColumn(name = "USER_ID")) //добавляем поле USER_ID связанное с name
    private Set<Address> listofaddr = new HashSet();

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<Address> getListofaddr() {
        return listofaddr;
    }

    public void setListofaddr(Set<Address> listofaddr) {
        this.listofaddr = listofaddr;
    }

    
}
