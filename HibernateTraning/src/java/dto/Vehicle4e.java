package dto;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "VEHICLE4e1")
public class Vehicle4e {

    @Id
    @GeneratedValue
    private int vehicleId;
    private String vehicleName;
    @ManyToMany (mappedBy = "vehiclesList")
    private Collection<UserDetails4e> userlist = new ArrayList<>();

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Collection<UserDetails4e> getUserlist() {
        return userlist;
    }

    public void setUserlist(Collection<UserDetails4e> userlist) {
        this.userlist = userlist;
    }

    

}
