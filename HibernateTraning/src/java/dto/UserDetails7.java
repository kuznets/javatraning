package dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@NamedQuery(name = "UserDetails7.byId", query="from UserDetails7 where userID=?") //первый вариант
@NamedNativeQuery(name = "UserDetails7.byName", query="select * from USER_DETAILS where userName=?", //второй вариант c нативным запросом
        resultClass = UserDetails7.class)
@Table (name = "USER_DETAILS")
public class UserDetails7 implements Serializable {

@Id
@GeneratedValue
private int userID;
private String userName;

public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
