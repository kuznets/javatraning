//Создаем таблицу User_Details_3b к которой присоединяем таблицу USER_ADDRESS_3b (делаем связные таблицы по USER_ID (one to many)
//В данном варианте мы создаем поле ADDRESS_ID с уникальным ID используя генератор "hilo-gen"
package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity(name = "User_Details_3b")
public class UserDetails3b implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;

    //C помощью аннотаций @GeneratedValue и @GenericGenerator задаем способ формирования новых ключей.
    @GenericGenerator(name = "hilo-gen", strategy = "hilo")// hilo auto id generator 
    @CollectionId(columns = {
        @Column(name = "ADDRESS_ID")}, generator = "hilo-gen", type = @Type(type = "long"))
    @ElementCollection //указываем что это будет коллекция
    @JoinTable(name = "USER_ADDRESS_3b", //даем кастомное имя таблице
            joinColumns = @JoinColumn(name = "USER_ID")) //добавляем поле USER_ID связанное с name
    private Collection<Address> listofaddr;

    public UserDetails3b() {
        this.listofaddr = new ArrayList<>();
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<Address> getListofaddr() {
        return listofaddr;
    }

    public void setListofaddr(Collection<Address> listofaddr) {
        this.listofaddr = listofaddr;
    }

}
