//Создаем таблицу User_Details_2, подключаем класс Address для задания различных адресов (домашный, рабочий, почтовый и пр.)
package dto;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "User_Details_2")
public class UserDetails2 implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;
//    @Embedded
//    private Address address ;
//    @AttributeOverride(name = "street",
//            column = @Column(name = "HOME_STREET_NAME"))
     @AttributeOverrides({ //переопределяем имена полей
        @AttributeOverride(name = "street",
                column = @Column(name = "HOME_STREET_NAME")),
        @AttributeOverride(name = "city",
                column = @Column(name = "HOME_CITY_NAME")),
        @AttributeOverride(name = "state",
                column = @Column(name = "HOME_STATE_NAME")),
        @AttributeOverride(name = "pincode",
                column = @Column(name = "HOME_PIN_CODE"))
    }
    )
    private Address homeaddress;
    private Address officeaddress;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

//    public Address getAddress() {
//        return address;
//    }
//
//    public void setAddress(Address address) {
//        this.address = address;
//    }
    public Address getHomeaddress() {
        return homeaddress;
    }

    public void setHomeaddress(Address homeaddress) {
        this.homeaddress = homeaddress;
    }

    public Address getOfficeaddress() {
        return officeaddress;
    }

    public void setOfficeaddress(Address officeaddress) {
        this.officeaddress = officeaddress;
    }

}
