package dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "VEHICLE4b1")
public class Vehicle4b {

    @Id
    @GeneratedValue
    private int vehicleId;
    private String vehicleName;
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE) //что делать если получили ошибку
    private UserDetails4b user;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public UserDetails4b getUser() {
        return user;
    }

    public void setUser(UserDetails4b user) {
        this.user = user;
    }

}
