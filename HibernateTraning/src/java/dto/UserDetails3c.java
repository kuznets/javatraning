package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity(name = "User_Details_3c")
public class UserDetails3c implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;

    @ElementCollection (fetch = FetchType.EAGER) //в случае EAGER у нас полноценный Java объект, 
                    //в случае LAZY используем proxy объект (обращается только когда требуется)
    @JoinTable(name = "USER_ADDRESS_3c", //даем кастомное имя таблице
            joinColumns = @JoinColumn(name = "USER_ID")) //добавляем поле USER_ID связанное с name
    private Collection<Address> listofaddr;

    public UserDetails3c() {
        this.listofaddr = new ArrayList<>();
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<Address> getListofaddr() {
        return listofaddr;
    }

    public void setListofaddr(Collection<Address> listofaddr) {
        this.listofaddr = listofaddr;
    }

}
