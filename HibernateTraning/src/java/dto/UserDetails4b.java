package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS4b1")
public class UserDetails4b implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private int userID;
    @Column(name = "USER_NAME")
    private String userName;
    @OneToMany (cascade=CascadeType.PERSIST) //cascade используется для PERSIST
    @JoinTable(name = "USER_VEHICLE4b1", joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "VEHICLE_ID")
    )
    private Collection<Vehicle4b> vehiclesList = new ArrayList(); //Лучше использовать List<Vehicle4b> ...
    
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<Vehicle4b> getVehiclesList() {
        return vehiclesList;
    }

    public void setVehiclesList(Collection<Vehicle4b> vehiclesList) {
        this.vehiclesList = vehiclesList;
    }
    
}
