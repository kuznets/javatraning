package servlets;

import dto.UserDetails5;
import dto.UserDetails7;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import persistence.HibernateUtil;

@WebServlet(name = "HbServlet7", urlPatterns = {"/hbtest7"})
public class HbServlet7 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String msg = "";
        Session session = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            session = sessionFactory.openSession();
            session.beginTransaction();

            for (int i = 0; i < 10; i++) {
                UserDetails7 user = new UserDetails7();
                user.setUserName("User #" + (i + 1));
                session.save(user);
            }

//            PATAMETER BINDING
            String requiredID = "5";
//            requiredID = "5 or 1 = 1";// sql injection
            requiredID = "5";
            String userName = "User #3";

//           Query query = session.createQuery("from UserDetails7 where userId = " + requiredID);
            //экранирование
//            Query query = session.createQuery("from UserDetails7 where userId = ?" + " or userName = ?");
//            query.setInteger(0, Integer.parseInt(requiredID));
//            query.setString(1, urName);
            //экранирование
//            Query query = session.createQuery("from UserDetails7 where userID = :userID" + " or userName = :userName");
//            query.setInteger("userID", Integer.parseInt(requiredID));
//            query.setString("userName", userName);
// NAMED QUERY
            //первый вариант
//            Query query = session.getNamedQuery("UserDetails7.byId");
//            query.setInteger(0, Integer.parseInt(requiredID));
            //второй вариант c нативным запросом
//            Query query = session.getNamedQuery("UserDetails7.byName");
//            query.setString(0, userName);
//            Criteria criteria = session.createCriteria(UserDetails7.class);
//            criteria.add(Restrictions.eq("userName", "User #5"))
//                    .add(Restrictions.gt("userID", 3)) //.add это AND
//                    .add(Restrictions.like("userName", "%User #1%")) //like поиск похожих вариантов
//                    .add(Restrictions.between("userID", 3, 30))
//                    .add(Restrictions.or(Restrictions.between("userID", 0, 3), Restrictions.between("userID", 7, 20)));
//            List<UserDetails7> list = (List<UserDetails7>) criteria.list();
//            msg += "<br>Result #3: " + "<br>";
//            for (UserDetails7 l : list) {
//                msg += l.getUserID() + " / " + l.getUserName() + "<br>";
//            }
            Criteria criteria = session.createCriteria(UserDetails7.class)
                    .setProjection(Projections.property("userID")) //Запрос вывести список по userID
//                    .setProjection(Projections.max("userID")) // только наибольший
//                    .setProjection(Projections.count("userID")) //колличество записей
                    .addOrder(Order.desc("userID")); //сортировать ...
            
            List<Integer> list = (List<Integer>) criteria.list();
            msg += "<br>Result #3: " + "<br>";
            for (int l : list) {
                msg += l + "<br>";
            }

//            List<UserDetails7> users = (List<UserDetails7>) query.list();
//            msg += "<br>Result #1: " + users.size() + "<br><br>";
//            for (UserDetails7 u : users) {
//                msg += u.getUserID()
//                        + " / " + u.getUserName() + "<br>";
//            }


            UserDetails7 exampleUser = new UserDetails7();
            exampleUser.setUserID(2); //null props and  PK - ignored
            exampleUser.setUserName("User #5");
            exampleUser.setUserName("User #1%");
             
            Example example = Example.create(exampleUser)//;
                    .excludeProperty("userName") //исключить свойство
                    .enableLike(); //включить свойство "похожее"
//            Criteria
             criteria = session.createCriteria(UserDetails7.class)
                    .add(example);
            
            List<UserDetails7> users = (List<UserDetails7>) criteria.list();
            
                        msg += "<br>Result #5: " + "<br>";
            for (UserDetails7 u : users) {
                msg += u.getUserID() 
                         + " / " + u.getUserName() + "<br>";
            }
            
        } catch (HibernateException e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
            }
            e.printStackTrace(System.err);
        }

        msg += "<br><br><br><br><br>";
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HbServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
