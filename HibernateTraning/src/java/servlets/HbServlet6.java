package servlets;

import dto.UserDetails5;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet(name = "HbServlet6", urlPatterns = {"/hbtest6"})
public class HbServlet6 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String msg = "";
        Session session = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            session = sessionFactory.openSession();
            session.beginTransaction();

            for (int i = 0; i < 10; i++) {
                UserDetails5 user = new UserDetails5();
                user.setUserName("User #" + (i + 1));
                session.save(user);
            }

//            //#1
            Query query = session.createQuery("from UserDetails5"); //указываем имя класса
            List<UserDetails5> users = (List<UserDetails5>) query.list();
//            msg += "Result #1: " + users.size() + "<br>";
//
//            for (UserDetails5 u : users) {
//                msg += u.getUserName() + "<br>";
//            }
//
//            //Pagenation #2
//            query.setFirstResult(5);
//            query.setMaxResults(4);
            users = (List<UserDetails5>) query.list();
//            msg += "<br>Result #2: " + users.size() + "<br>";
//
//            //#3
//            query = session.createQuery("from UserDetails5 where userId > 5");
//            users = query.list();
//            msg += "<br>Result #3: " + users.size() + "<br>";
//            
//            //#3.1
//             query = session.createQuery("select userName from UserDetails5");
//            List<String> names = (List<String>) query.list();
//            for (String name : names) {
//                msg += "<br>Result #3.1: " + name + "<br>";
//            }

              //#3.2 sql inject
            query = session.createQuery("from UserDetails5 where userId > 5 or 1=1");
            users = query.list();
            msg += "<br>Result #3: " + users.size() + "<br>";
//
//            //#4
//            query = session.createQuery("select max(userID) from UserDetails5");
//            List<Integer> num = (List<Integer>) query.list();
//            msg += "<br>Result #4: " + num.get(0) + "<br>";

            //#5
//            query = session.createQuery("select new map (userID, userName) from UserDetails5");
//            List rows = query.list();
//            msg += "<br>Result #5: " + rows.size() + "<br>";
//
//            Iterator rowsIt = rows.iterator();
//            while (rowsIt.hasNext()) {
//                msg += (Map) rowsIt.next() + "<br>";
//            }
//            #6 Проверить! get(0) нужно извлечь
            query = session.createQuery("select userID, userName from UserDetails5");
            List<Object[]> list = (List<Object[]>) query.list();
            for (Object[] obj : list) {
                msg += obj[0] + "<br>" + " / " + obj[1] + "<br>";
            }
            
            //#7
            

        } catch (HibernateException e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
            }
            e.printStackTrace(System.err);
        }

        msg += "<br><br><br><br><br>";
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HbServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
