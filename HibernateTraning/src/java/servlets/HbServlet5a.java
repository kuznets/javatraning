package servlets;

import dto.UserDetails5;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet(name = "HbServlet5a", urlPatterns = {"/HbServlet5a"})
public class HbServlet5a extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String msg = "";
        Session session = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            session = sessionFactory.openSession();
            session.beginTransaction();

            for (int i = 0; i < 10; i++) {
                UserDetails5 user = new UserDetails5();
                user.setUserName("User #" + (i + 1));
                session.save(user);
            }

            msg += "Ok." + "<br>";

//            UserDetails5 userDel = (UserDetails5) session.get(UserDetails5.class, 5);
//            session.delete(userDel);
////            for (int i = 0; i < 7; i++) {
////                UserDetails5 user = (UserDetails5) session.get(UserDetails5.class, i + 1);
////                msg += user.getUserName() + "<br>";
////            }
//            session = sessionFactory.openSession();
//            session.beginTransaction();
            UserDetails5 user = (UserDetails5) session.get(UserDetails5.class, 6);
            user.setUserName("New Name");
            session.update(user);

            session.getTransaction().commit();

            session.close();

        } catch (HibernateException e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
            }
            e.printStackTrace(System.err);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HbServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
