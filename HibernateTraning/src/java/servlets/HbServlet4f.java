package servlets;

import dto.Vehicle_2wheels;
import dto.Vehicle_4wheels;
import dto.Vehicle_basic;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet("/hbtest4f")
public class HbServlet4f extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       Vehicle_basic vehicle = new Vehicle_basic();
        vehicle.setVehicleName("Vehicle");

        Vehicle_2wheels bike = new Vehicle_2wheels();
        bike.setVehicleName("Bike");
        bike.setSteeringHandle("Bike Handle");

        Vehicle_4wheels car = new Vehicle_4wheels();
        car.setVehicleName("Car");
        car.setSteeringWheel("Car Wheel");

        String msg = "";
        Session session = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            session = sessionFactory.openSession();
            session.beginTransaction();

            session.save(vehicle);
            session.save(bike);
            session.save(car);

            session.getTransaction().commit();
            session.close();

            msg = "OK.";

        } catch (HibernateException e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
            }
            e.printStackTrace(System.err);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HbServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
