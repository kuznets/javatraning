//one to one
package servlets;

import dto.UserDetails4a;
import dto.Vehicle;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet("/hbServlet4a")
public class HbServlet4a extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        UserDetails4a  user = new UserDetails4a();
        user.setUserName("Name 1");
           
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleName("Car");

        user.setVehicle(vehicle);
        vehicle.setUser(user);
        
        Session session = null;
        String msg = "";
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            session.save(user);
            session.save(vehicle);
            
            session.getTransaction().commit();
            
            user = (UserDetails4a) session.get(UserDetails4a.class, 1);
            if (user != null) {
                msg += user.getUserName() + "<br>" + user.getVehicle().getVehicleName();
            }
            session.close();
            
        } catch (HibernateException e) {
//            if (session != null) {
//                session.getTransaction().rollback();
//                session.close();
//            }
            e.printStackTrace(System.err);
        }

        request.setAttribute("msg", msg);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        
        
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet HbServlet2</title>");  
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet HbServlet2 at " + request.getContextPath () + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

}
