package servlets;

import dto.UserDetails5;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet(name = "HbServlet5b", urlPatterns = {"/hbtest5b"})
public class HbServlet5b extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String msg = "";
        Session session = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

            session = sessionFactory.openSession();
            session.beginTransaction();

            UserDetails5 user = new UserDetails5();
            user.setUserName("User #1");
            // transient object

            session.save(user);
//session.getTransaction().commit();
//            // persistent object
//            user.setUserName("User #2");
//            user.setUserName("User #3");
            
//            msg += user.getUserName() + "<br>";

            session.delete(user);
            // transient object
            
            msg += user.getUserName() + "<br>";
//
            session.save(user);
            // persistent object
            msg += user.getUserName() + "<br>";
//
//            session.getTransaction().commit();
//            session.close();
//
//            // detached object
//            user.setUserName("User #4");
//            msg += user.getUserName() + "<br>";
//
//            session = sessionFactory.openSession();
//            session.beginTransaction();
//
//            user = (UserDetails5) session.get(UserDetails5.class, 1);
//            // persistent object
//            msg += user.getUserName() + "<br>";
//            session.getTransaction().commit();
//            session.close();
//            // detached object
//            msg += user.getUserName() + "<br>";
//
//            user.setUserName("User #5");
//
//            session = sessionFactory.openSession();
//            session.beginTransaction();
//
//            session.update(user);
//            // persistent object
//            msg += user.getUserName() + "<br>";
//            
//            user.setUserName("User #6");
//             msg += user.getUserName() + "<br>";
//            
//            session.getTransaction().commit();
//            session.close();

        } catch (HibernateException e) {
            if (session != null) {
                session.getTransaction().rollback();
                session.close();
            }
            e.printStackTrace(System.err);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HbServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
