//one to one
package servlets;

import dto.UserDetails4e;
import dto.Vehicle4e;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet("/testHibernate4e")
public class HbServlet4e extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        UserDetails4e  user1 = new UserDetails4e();
        UserDetails4e  user2 = new UserDetails4e();
        user1.setUserName("Name 1");
        user2.setUserName("Name 2");
           
        Vehicle4e vehicle1 = new Vehicle4e();
        Vehicle4e vehicle2 = new Vehicle4e();
        vehicle1.setVehicleName("Car1");
        vehicle2.setVehicleName("Car2");

        user1.getVehiclesList().add(vehicle1);
        user1.getVehiclesList().add(vehicle2);
        user2.getVehiclesList().add(vehicle2);
        
        vehicle1.getUserlist().add(user1);
        vehicle2.getUserlist().add(user1);
        vehicle2.getUserlist().add(user2);
        
        Session session = null;
        String msg = "";
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            session.save(user1);
            session.save(user2);
            session.save(vehicle1);
            session.save(vehicle2);
            
            session.getTransaction().commit();
            
            user1 = (UserDetails4e) session.get(UserDetails4e.class, 1);
            if (user1 != null) {
                msg += user1.getUserName() + "<br>";
                List<Vehicle4e> list = (List<Vehicle4e>) user1.getVehiclesList();
                Vehicle4e v = list.get(0);
                msg += v.getVehicleName();
            }
            session.close();
            
        } catch (HibernateException e) {
//            if (session != null) {
//                session.getTransaction().rollback();
//                session.close();
//            }
            e.printStackTrace(System.err);
        }

        request.setAttribute("msg", msg);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        
        
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet HbServlet2</title>");  
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet HbServlet2 at " + request.getContextPath () + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

}
