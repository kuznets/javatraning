//Создаем таблицу User_Details_3b к которой присоединяем таблицу USER_ADDRESS_3b (делаем связные таблицы по USER_ID (one to many)
//Таблица не содержит уникального идентификатора
package servlets;

import dto.Address;
import dto.UserDetails3c;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet("/hbServlet3c")
public class HbServlet3c extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        UserDetails3c  user = new UserDetails3c();
        user.setUserName("Name 1");
        
        Address addr = new Address();
        addr.setCity("city");
        addr.setPincode("code");
        addr.setState("state");
        addr.setStreet("street");
        
        Address addr2 = new Address();
        addr2.setCity("citt2");
        addr2.setPincode("code2");
        addr2.setState("state2");
        addr2.setStreet("street2");
        
        user.getListofaddr().add(addr);
        user.getListofaddr().add(addr2);
        
        Session session = null;
        String msg = "";
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            session.save(user);
            
            session.getTransaction().commit();
            
            user = (UserDetails3c) session.get(UserDetails3c.class, 1);
            if (user != null) {
                msg += user.getUserName();
            }
            msg += "<br>" + user.getListofaddr().size(); //получаем колличество записей
            session.close();
            
        } catch (HibernateException e) {
//            if (session != null) {
//                session.getTransaction().rollback();
//                session.close();
//            }
            e.printStackTrace(System.err);
        }

        request.setAttribute("msg", msg);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        
        
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet HbServlet2</title>");  
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet HbServlet2 at " + request.getContextPath () + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

}
