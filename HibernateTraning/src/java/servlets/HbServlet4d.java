//one to one
package servlets;

import dto.UserDetails4d;
import dto.Vehicle4d;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.HibernateUtil;

@WebServlet("/testHibernate4d")
public class HbServlet4d extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        UserDetails4d  user = new UserDetails4d();
        user.setUserName("Name 1");
           
        Vehicle4d vehicle1 = new Vehicle4d();
        Vehicle4d vehicle2 = new Vehicle4d();
        vehicle1.setVehicleName("Car1");
        vehicle2.setVehicleName("Car2");

        user.getVehiclesList().add(vehicle1);
        user.getVehiclesList().add(vehicle2);
        
        vehicle1.setUser(user);
        vehicle2.setUser(user);
        
        Session session = null;
        String msg = "";
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            session.save(user);
            session.save(vehicle1);
            session.save(vehicle2);
            
            session.getTransaction().commit();
            
            user = (UserDetails4d) session.get(UserDetails4d.class, 1);
            if (user != null) {
                msg += user.getUserName() + "<br>";
                List<Vehicle4d> list = (List<Vehicle4d>) user.getVehiclesList();
                Vehicle4d v = list.get(0);
                msg += v.getVehicleName();
            }
            session.close();
            
        } catch (HibernateException e) {
//            if (session != null) {
//                session.getTransaction().rollback();
//                session.close();
//            }
            e.printStackTrace(System.err);
        }

        request.setAttribute("msg", msg);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        
        
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet HbServlet2</title>");  
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet HbServlet2 at " + request.getContextPath () + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

}
