angular.module("mainModule.script", [])

    .controller('mainController', function ($scope, $http) {
        $scope.url = 'http://localhost:8090/api/';
        $scope.messages = [];
        $scope.showEditFrame = false;
        // $scope.newMessage = {};


        //Get all messages
        $http.get($scope.url + 'messages').success(function (data) {
            $scope.messages = data;
        })

        //Add one message
        $scope.addMessage = function (newMessage) {
            $http({
                method: 'POST',
                url: $scope.url + 'message/',
                data: newMessage,
                headers: {'Content-Type': 'application/json'}
            })
            .success(function (response) {
                $scope.messages.push(response);
            }, function (response) {
                console.log(response);
            });
        }

        //Update the item
        $scope.editMessage = function (data) {
            $scope.current = angular.copy(data);
            $scope.showEditFrame = true;
        }

        $scope.saveEditedMessage = function (data) {
            $http({
                method: 'PUT',
                url:  $scope.url + 'message/' + data.id,
                data: data,
                headers: {'Content-Type': 'application/json'}
            }).success(function (response) {
                for (var i = 0; i < $scope.messages.length; i++) {
                    for (var key in $scope.messages[i]) {
                        if ($scope.messages[i]['id'] === data.id) {
                            $scope.messages.splice(i, 1, data);
                        }
                    }
                }
                $scope.showEditFrame = false;
            }, function (response) {
                console.log(response);
            });
        }

        //Delete the item
        $scope.deleteMessage = function (id) {
            $http.delete($scope.url + 'message/' + id).success(function (response) {
                for (var i = 0; i < $scope.messages.length; i++) {
                    for (var key in $scope.messages[i]) {
                        if ($scope.messages[i]['id'] === id) {
                            $scope.messages.splice(i, 1);
                            break;
                        }
                    }
                }
            }, function (response) {
                console.log("Return error");
            });
        }

        $scope.closeEditFrame = function () {
            $scope.showEditFrame = false;
        }
    })