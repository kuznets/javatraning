package com.sttech.springrest.api;

import com.sttech.springrest.exception.DataNotFoundException;
import com.sttech.springrest.model.Message;
import com.sttech.springrest.service.MessageService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Rest controller return messages from database.
 */

@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * Get all messages
     * @return List of messages
     */
    @RequestMapping(
            value = "/api/messages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getAllMessages(){
        return messageService.findAll();
    }

    /**
     * Get one message for id
     * @param id id
     * @return Message message
     */
    @RequestMapping(
            value = "/api/message/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> getMessage(@PathVariable Long id){
        return new ResponseEntity<>(messageService.findOne(id), HttpStatus.OK);
    }

    /**
     * Add one message
     * @param message message
     * @return Message message
     */
    @RequestMapping(
            value = "/api/message",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addMessage(@RequestBody Message message) {
        if (message == null) {
            throw  new DataNotFoundException("The message should not be empty.");
        }
        return new ResponseEntity<>(messageService.save(message), HttpStatus.CREATED);
    }

    /**
     * Add array of messages
     * @param messages messages
     * @return Object of messages
     */
    @RequestMapping(
            value = "api/messages",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addMessages(@RequestBody List<Message> messages) {
        return new ResponseEntity<Object>(messageService.saveArray(messages), HttpStatus.CREATED);
    }

    /**
     * Update the item
     * @param id id
     * @param message message
     * @return message
     */
    @RequestMapping(
            value = "api/message/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateMessage(@PathVariable Long id, @RequestBody Message message) {
        return new ResponseEntity<>(messageService.update(id, message), HttpStatus.OK);
    }

    /**
     * Delete the item
     * @param id id
     * @return HttpStatus
     */
    @RequestMapping(
            value = "api/message/{id}",
            method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMessage(@PathVariable Long id) {
        messageService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    public ResponseEntity<?> deleteMessage(@PathVariable Long id) {
//        if (!messageService.delete(id)) {
//            throw  new DataNotFoundException("Data with id=" + id + " not found.");
//        }
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}
