package com.sttech.springrest.service;

import com.sttech.springrest.exception.DataNotFoundException;
import com.sttech.springrest.model.Message;
import com.sttech.springrest.repository.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MessageServiceImp implements MessageService{

	private final MessageRepository messageRepository;

	public MessageServiceImp(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	/**
	 * Find all messages
	 * @return List of all messages
	 */
	@Override
	public List<Message> findAll() {
	    return messageRepository.findAll();
	}

	/**
	 * Find one message by id
	 * @param id id
	 * @return Message message
	 */
	@Override
	public Message findOne(Long id) {
		Message message = messageRepository.findOne(id);
		if (message == null) {
			throw  new DataNotFoundException("Data with id=" + id + " not found.");
		}
	    return message;
	}

	/**
	 * Add one message
	 * @param message message
	 * @return message message
	 */
	@Override
	public Message save(Message message) {
		if (message == null) {
			throw  new DataNotFoundException("The message should not be empty.");
		}
		return messageRepository.save(message);
	}

	/**
	 * Add array of messages
	 * @param messages
	 * @return List of saved messages
	 */
	@Override
	public List<Message> saveArray(List<Message> messages) {
		if (messages == null) {
			throw  new DataNotFoundException("The list of messages is null.");
		}
		if (messages.isEmpty()) {
			throw  new DataNotFoundException("The list of messages should not be empty.");
		}
		return messageRepository.save(messages);
	}

	/**
	 * Update one message by id
	 * @param id
	 * @param message
	 * @return Updated message
	 */
	@Override
	public Message update(Long id, Message message){
		Message currentMessage = messageRepository.findOne(id);
		if (currentMessage == null) {
			throw  new DataNotFoundException("Data with id=" + id + " not found.");
		}
		currentMessage.setId(currentMessage.getId());
		currentMessage.setAuthor(message.getAuthor());
		currentMessage.setMessage(message.getMessage());
		currentMessage.setCreated(message.getCreated());
		return messageRepository.save(currentMessage);
	}

	/**
	 * Delete message by id
	 * @param id
	 * @return true if deleted and custom exception if not
	 */
	@Override
	public void delete(Long id) {
		messageRepository.delete(id);
	}

}
