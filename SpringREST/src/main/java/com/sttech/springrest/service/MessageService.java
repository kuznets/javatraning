package com.sttech.springrest.service;

import com.sttech.springrest.model.Message;

import java.util.List;

public interface MessageService {

    List<Message> findAll();

    Message findOne(Long id);

    Message save(Message message);

    List<Message> saveArray(List<Message> messages);

    Message update(Long id, Message message);

    void delete(Long id);

}
