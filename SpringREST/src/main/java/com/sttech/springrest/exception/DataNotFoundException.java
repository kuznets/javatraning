package com.sttech.springrest.exception;

/**
 * Custom exception
 */
public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message) {
        super(message);
    }
}