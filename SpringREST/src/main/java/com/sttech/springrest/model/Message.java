package com.sttech.springrest.model;

import javax.persistence.*;
import java.util.Date;

/**
 * ORM data model Message
 */

@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;
    @Column(name = "MESSAGE")
    private String message;
    @Column(name = "AUTHOR")
    private String author;
    @Column(name = "CREATED")
    @Temporal(TemporalType.DATE)
    private Date created = new Date();

    public Message() {
    }

    /**
     * Create instance with reqiered fields.
     * @param id id
     * @param message message
     * @param author author
     * @param created current date
     */
    public Message(Long id, String message, String author, Date created) {
        this.id = id;
        this.message = message;
        this.author = author;
        this.created = created;
    }

    /**
     * Get the message id
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the message text
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message text to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get the message create date
     * @return created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the message create date to set
     */
    public void setCreated(Date created) { this.created = created; }

    /**
     * Get the message author name
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the message author name to set
     */
    public void setAuthor(String author) { this.author = author; }

}
