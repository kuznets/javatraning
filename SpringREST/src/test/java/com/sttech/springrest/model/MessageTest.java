package com.sttech.springrest.model;

import org.junit.Test;
import java.util.Date;
import static org.junit.Assert.*;

public class MessageTest {
    private final Date CREATED = new Date();
    private final long ID = 1L;
    private final Message testedMessage = new Message(1L, "test message", "test author", CREATED);
    private final Message testedMessage2 = new Message();

    @Test
    public void getId() throws Exception {
        assertEquals(1, testedMessage.getId());
    }

    @Test
    public void setId() throws Exception {
        testedMessage2.setId(ID);
        assertEquals(1, testedMessage2.getId());
    }

    @Test
    public void getMessage() throws Exception {
        assertEquals("test message", testedMessage.getMessage());
    }

    @Test
    public void setMessage() throws Exception {
        testedMessage2.setMessage("text");
        assertEquals("text", testedMessage2.getMessage());
    }

    @Test
    public void getCreated() throws Exception {
        assertEquals(CREATED, testedMessage.getCreated());
    }

    @Test
    public void setCreated() throws Exception {
        testedMessage2.setCreated(CREATED);
        assertEquals(CREATED, testedMessage2.getCreated());
    }

    @Test
    public void getAuthor() throws Exception {
        assertEquals("test author", testedMessage.getAuthor());
    }

    @Test
    public void setAuthor() throws Exception {
        testedMessage2.setAuthor("name");
        assertEquals("name", testedMessage2.getAuthor());
    }

}