package com.sttech.springrest.service;

import com.google.common.collect.ImmutableList;
import com.sttech.springrest.exception.DataNotFoundException;
import com.sttech.springrest.model.Message;
import com.sttech.springrest.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImpTest {

    private final long ID = 1L;
    private final Date CREATED = new Date();
    private final Message  MESSAGE = new Message(ID, "text mesage", "author name", CREATED);
    private final List<Message> MESSAGE_LIST = new ArrayList<>();

    @Mock
    private MessageRepository messageRepository;
    @InjectMocks
    private MessageServiceImp messageServiceImp;

    @Test
    public void findAll() throws Exception {
        //prepare
        when(messageRepository.findAll()).thenReturn(ImmutableList.of());
        //testing
        List<Message> messages = messageServiceImp.findAll();
        //validate
        verify(messageRepository).findAll();
    }

    @Test
    public void findOne() throws Exception {
        //prepare
        when(messageRepository.findOne(ID)).thenReturn(MESSAGE);
        //testing
        Message testMessage = messageServiceImp.findOne(ID);
//        //validate
        verify(messageRepository).findOne(ID);
        assertEquals(MESSAGE, messageServiceImp.findOne(ID));
    }

    @Test(expected = DataNotFoundException.class)
    public void findOneIsNullThenException() {
        //prepare
        when(messageServiceImp.findOne(2L));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("Data with id=2 not found.");
    }

    @Test
    public void save() throws Exception {
        //prepare
        when(messageRepository.save(MESSAGE)).thenReturn(MESSAGE);
        //testing
        Message testMessage = messageServiceImp.save(MESSAGE);
        //validate
        verify(messageRepository).save(MESSAGE);
        assertEquals(MESSAGE, messageServiceImp.save(MESSAGE));
    }

    @Test(expected = DataNotFoundException.class)
    public void saveIsNullThenException() {
        //prepare
        when(messageServiceImp.save(null));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("The message should not be empty.");
    }

    @Test
    public void saveArray() throws Exception {
        //prepare
        MESSAGE_LIST.add(MESSAGE);
        when(messageRepository.save(MESSAGE_LIST)).thenReturn(MESSAGE_LIST);
        //testing
        List<Message> testMessage = messageServiceImp.saveArray(MESSAGE_LIST);
        //validate
        verify(messageRepository).save(MESSAGE_LIST);
        assertEquals(MESSAGE_LIST, messageServiceImp.saveArray(MESSAGE_LIST));
    }

    @Test(expected = DataNotFoundException.class)
    public void saveArrayIsNullThenException() {
        //prepare
        when(messageServiceImp.saveArray(null));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("The list of messages is null.");
    }

    @Test(expected = DataNotFoundException.class)
    public void saveArrayIsEmptyThenException() {
        //prepare
        when(messageServiceImp.saveArray(MESSAGE_LIST));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("The list of messages should not be empty.");
    }

    @Test
    public void update() throws Exception {
        //prepare
        when(messageRepository.findOne(ID)).thenReturn(MESSAGE);
        when(messageRepository.save(MESSAGE)).thenReturn(MESSAGE);
        //testing
        Message testMessage = messageServiceImp.update(ID, MESSAGE);
        //validate
        verify(messageRepository).findOne(ID);
        verify(messageRepository).save(MESSAGE);
        assertEquals(MESSAGE, messageServiceImp.update(ID, MESSAGE));
    }

    @Test(expected = DataNotFoundException.class)
    public void updateIsNullThenException() {
        //prepare
        when(messageServiceImp.update(ID, MESSAGE));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("Data with id=" + ID + " not found.");
    }

    @Test
    public void delete() throws Exception {
        //testing
        messageServiceImp.delete(ID);
        //validate
        verify(messageRepository).delete(ID);
    }
}