package com.sttech.springrest.api;

import com.google.common.collect.ImmutableList;
import com.sttech.springrest.exception.DataNotFoundException;
import com.sttech.springrest.model.Message;
import com.sttech.springrest.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.googlecode.catchexception.apis.BDDCatchException.*;
import static org.assertj.core.api.BDDAssertions.then;


@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {

    private final long ID = 1L;
    private final Date CREATED = new Date();
    private final Message  MESSAGE = new Message(ID, "text mesage", "author name", CREATED);
    private final List<Message> MESSAGE_LIST = new ArrayList<>();

    @Mock
    private MessageService messageService;
    @InjectMocks
    MessageController messageController;

    @Test
    public void getAllMessages() throws Exception {
        //prepare
        when(messageController.getAllMessages()).thenReturn(ImmutableList.of());
        //testing
        List<Message> messages = messageController.getAllMessages();
        //validate
        verify(messageService).findAll();
    }

    @Test
    public void getMessage() throws Exception {
        //prepare
        when(messageService.findOne(ID)).thenReturn(MESSAGE);
        //testing
        ResponseEntity<Message> testMessage = messageController.getMessage(ID);
        //validate
        verify(messageService).findOne(ID);
    }

    @Test
    public void addMessage() throws Exception {
        //prepare
        when(messageService.save(MESSAGE)).thenReturn(MESSAGE);
        //testing
        ResponseEntity<Message> testMessage = (ResponseEntity<Message>) messageController.addMessage(MESSAGE);
        //validate
        verify(messageService).save(MESSAGE);
    }

    @Test(expected = DataNotFoundException.class)
    public void addMessageIfNotFoundThenException() {
        //prepare
        when(messageController.addMessage(null));
        then(caughtException())
                .isInstanceOf(DataNotFoundException.class)
                .hasMessageContaining("The message should not be empty.");
    }

    @Test
    public void addMessages() throws Exception {
        //prepare
        MESSAGE_LIST.add(MESSAGE);
        when(messageService.saveArray(MESSAGE_LIST)).thenReturn(MESSAGE_LIST);
        //testing
        ResponseEntity<Object> testMessages = (ResponseEntity<Object>) messageController.addMessages(MESSAGE_LIST);
        //validate
        verify(messageService).saveArray(MESSAGE_LIST);
    }

    @Test
    public void updateMessage() throws Exception {
        //prepare
        when(messageService.update(ID, MESSAGE)).thenReturn(MESSAGE);
        //testing
        ResponseEntity<Message> testMesssage = (ResponseEntity<Message>) messageController.updateMessage(ID, MESSAGE);
        //validate
        verify(messageService).update(ID, MESSAGE);
    }

    @Test
    public void deleteMessage() throws Exception {
        //testing
        ResponseEntity<Message> testMessage = (ResponseEntity<Message>) messageController.deleteMessage(ID);
        //validate
        verify(messageService).delete(ID);
    }

    @Test
    public void InvocationCount() throws Exception {
        //prepare
        MESSAGE_LIST.add(MESSAGE);

        // call methods in mock
//        messageController.getAllMessages();
//        messageController.getAllMessages();
        messageService.findAll();
        messageService.findAll();

        messageController.getMessage(ID);
        messageController.getMessage(ID);

        messageController.addMessage(MESSAGE);
        messageController.addMessage(MESSAGE);
        messageController.addMessage(MESSAGE);

        messageController.addMessages(MESSAGE_LIST);
        messageController.addMessages(MESSAGE_LIST);

        messageController.updateMessage(ID, MESSAGE);
        messageController.updateMessage(ID, MESSAGE);
        messageController.updateMessage(ID, MESSAGE);

        messageService.delete(ID);

        // verify method calls
        verify(messageService, times(2)).findAll();
        verify(messageService, times(2)).findOne(ID);
        verify(messageService, times(3)).save(MESSAGE);
        verify(messageService, times(2)).saveArray(MESSAGE_LIST);
        verify(messageService, times(3)).update(ID, MESSAGE);
        verify(messageService, times(1)).delete(ID);
    }

}