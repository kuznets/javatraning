package com.sttech.maventraning;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("rest")
public class RestSample {
    
    //http://localhost:8084/MavenTraning/websources/rest/
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
        return "Hey, This is Jersey JAX-RS !";
    }
    
    // http://localhost:8080/JRESTBasics_M/webresources/thetest/getXML
    @Path("/getXML") 
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Welcome getXML()
    {
        Welcome welcome = new Welcome();
        welcome.setDomain("www.yandex.ru");
        welcome.setMessage("XML Example");
        return welcome;
    }
    
//    http://localhost:8080/JRESTBasics_M/webresources/thetest/getJSON
    @Path("/getJSON") 
    @GET
    @Produces({MediaType.APPLICATION_JSON}) 
    public Student getJSON()
    {
        Student s = new Student();
        s.setName("Qwerty");
        s.setAge(22);
        
//        Gson gson = new Gson();
//        String output = gson.toJson(s);

        return s;//Response.status(200).entity(output).build();
    }
    
//   http://localhost:8084/JRESTBasics_M/webresources/thetest/getjson2
    @GET
    @Path("/getjson2")
    @Produces(MediaType.APPLICATION_JSON)
    public Welcome simplejson() {
        Welcome welcome = new Welcome();
        welcome.setDomain("www.yandex.ru");
        welcome.setMessage("JSON Example");
        return welcome;
    }


}
