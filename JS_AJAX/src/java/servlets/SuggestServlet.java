package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SuggestServlet", urlPatterns = {"/SuggestServlet"})
public class SuggestServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String msg = "";
        if (request.getParameter("search") != null) {
            String search = request.getParameter("search");
            
            if (search.startsWith("a")){
                msg = "Apple<br><img src='images/1.jpeg'/>\nABB<br><img src='images/2.jpeg'/>";
            }
            else if (search.startsWith("b")){
                msg = "Ball<br><img src='images/3.jpeg' />\nBnjk<br><img src='images/4.jpe' />";
            }
            
//            switch (search) {
//                case "q":
//                    out.println("");
//                    break;
//                case "w":
//                    out.println("images/2.jpeg");
//                    break;
//                case "e":
//                    out.println("images/3.jpeg");
//                    break;
//                case "r":
//                    out.println("images/4.jpg");
//                    break;
//                default:
//                    break;
//            }
        }

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println(msg);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
