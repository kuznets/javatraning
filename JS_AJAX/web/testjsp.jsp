
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        # of visits: 
        <%
            int count = 0;

            if (session.getAttribute("count") != null) {
                count = (Integer) session.getAttribute("count");
            }

            count++;
            session.setAttribute("count", count);
            out.println(count);

            if (request.getParameter("name") != null && request.getParameter("lastname") != null) {
                out.println("<h1>Hi " + request.getParameter("name") + " " + request.getParameter("lastname") + "!</h1>");
            }


        %>
    </body>
</html>
