<%@page import="java.util.Iterator"%>
<%@page import="shoping_cart.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= namepage("123")%></title>
        <script>

            function validateAction(x) {
                var e = document.getElementById("fff");
                e.value = x;
            }

            function validateNumber(e) {
                if (e.value < 1)
                    e.value = 1;
            }

        </script>
    </head>
    <body>
        <h1><%= request.getRemoteAddr()%></h1>
        <%! private String namepage(String s) {
                return s + "QWERTY";
            }
        %> 
        <table style=" text-align: left; width: 55%; background-color:
               rgb(153, 255, 153); margin-left: auto; margin-right: auto;"
               border="1" cellpadding="2" cellspacing="2">
            <form action="shop" method="get">
                <tr><td style="vertical-align: top; text-align: center;"
                        vertical-align:=""><br>
                        Enter product code
                        <input type="number" name="code" value="" onchange="validateNumber(this)" 
                               placeholder="Enter product code" />
                    </td></tr><tr><td style="vertical-align: top; text-align: center;"
                                  vertical-align:=""><br>
                        Enter product name
                        <input type="text" name="name" value="" placeholder="Enter product name" />
                    </td></tr><tr><td style="vertical-align: top; text-align: center;"
                                  vertical-align:=""><br>
                        Enter product description
                        <input type="text" name="description" value="" placeholder="Enter product description" />
                    </td></tr><tr><td style="vertical-align: top; text-align: center;"
                                  vertical-align:=""><br>
                        Enter product price
                        <input type="number" name="price" value="" onchange="validateNumber(this)"
                               placeholder="Enter product price" />
                    </td></tr><tr><td style="vertical-align: top; text-align: center;"
                                  vertical-align:=""><br>
                        <input type="hidden" name="action" id="fff" value="" /> 
                        <input type="submit" value="ADD"  onclick="validateAction(this.value)" />
                        <input type="submit" value="REMOVE"  onclick="validateAction(this.value)" />
                        <input type="submit" value="EDIT" onclick="validateAction(this.value)" />

                    </td></tr>
            </form>
            <br>
            <%
                Product product;
                if (application.getAttribute("productList") != null) {
                    ArrayList<Product> productList = (ArrayList<Product>) application.getAttribute("productList");
            %>

            <table style="
                   text-align: left;
                   width: 35%;
                   background-color:rgb(255, 255, 153);
                   margin-left: auto;
                   margin-right: auto;
                   border: 1px;
                   "
                   cellpadding= "2" cellspacing="2" >
                <tr>
                    <th>
                        ID Code
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Описание
                    </th>   
                    <th>
                        Цена
                    </th>
                </tr>
                <%
                    Iterator<Product> iter = productList.iterator();
                    while (iter.hasNext()) {
                        product = iter.next();
                %>
                <tr>
                    <td><%= product.getCode()%></td>
                    <td><%= product.getName()%></td>
                    <td><%= product.getDescription()%></td>
                    <td><%= product.getPrice()%></td>
                </tr>
                <%}%>
            </table>

            <%}%>

    </body>
</html>
