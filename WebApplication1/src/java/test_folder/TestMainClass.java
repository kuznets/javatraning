package test_folder;

public class TestMainClass {

    public static void main(String[] args) {
        StaticTestClass.countStatic += 5;
        System.out.println("Static value " + StaticTestClass.countStatic);
        System.out.println("Static method " + StaticTestClass.calculateStatic(1, 3));
        
        StaticTestClass st = new StaticTestClass();
        st.count += 3;
        System.out.println("Count value " + st.count);
        System.out.println("Static method " + st.calculate(2, 6));
    }

}
