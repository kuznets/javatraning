package test_folder;


public class StaticTestClass {
    int count;
    static int countStatic;

    public StaticTestClass() {
        count = 0;
        countStatic = 0;
    }
    
    public int calculate(int a, int b) {
        return a*b;
    }
    
    static int calculateStatic(int a, int b) {
        return a*b;
    }
}
