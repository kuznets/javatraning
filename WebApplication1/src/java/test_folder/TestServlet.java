package test_folder;

import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String firstName = request.getParameter("firstName");
        int age;
        String ageMsg = new String();
        try {
            //Получаем возраст и преобразуем число в строку
            age = Integer.parseInt(request.getParameter("age"));
            if (age <= 0) {
                System.out.println("age <= 0");
                ageMsg = "You must be older then 0 ears!";
                age = 0;
            }
        } catch (NumberFormatException ex) {
            age = 0;
            ex.printStackTrace(System.err);
            System.out.println("Error");
        }

//        String output = arrayToOneWordsColumn(request.getParameter("textArea"));
//        String output = sortToSeveralWordsColumn(request.getParameter("textArea"));
//        String output = sortToSeveralWordsColumn1(request.getParameter("textArea"));
//        String output = collectionsPreview(request.getParameter("textArea"));
//        String output = collectionsMapsPreview2(request.getParameter("textArea"));
//        String output = collectionsMapsPreview1(request.getParameter("textArea")); // ОШИБКА!!!
//        String output = collectionsMapsPreview(request.getParameter("textArea")); 
//        String output = collectionsTreeMapsPreview(request.getParameter("textArea"));
//        String output = collectionsLinkedHashMapsPreview(request.getParameter("textArea"));
//        String output = collectionsLinkedHashMapsPreview1(request.getParameter("textArea"));
        String output = collectionsLinkedHashMapsPreview2(request.getParameter("textArea"));
//        String output = collectionsLinkedHashMapsPreview3(request.getParameter("textArea"));
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestServlet</title>");
            out.println("</head>");
            out.println("<body>");
//            out.println("<h1>Hello " + firstName + "</h1>");
//            if (age > 0) {
//                out.println("<text>Your age is: " + age + " </text>");
//            }
//            if (ageMsg.length() > 0) {
//                out.println(ageMsg);
//            }
//            out.println(cleanText(request.getParameter("textArea")));
//            out.println("<h1>Count: " + sessionCounter(request) + "</h1>"); //Вызываем метод и выводим значение
            out.println(output);
            out.println("</body>");
            out.println("</html>");
////////////////////////////////////////////////////////////////////////////////
//            doJob1(8);
//            calculator(5, 3, "+");
//            incAndDec(10);
//            if (palendroid("aba")) {
//                System.out.println("true");
//            } else {
//                System.out.println("false");
//            }
//            System.out.println(sumarr());
//            sumarrWeb(response);
//            sumWeb(response.getWriter());
        }
    }
    
    //Коллекция в коллекции
    private String collectionsLinkedHashMapsPreview3(String text) {
        String arr[] = cleanText(text).split(" ");
//        TreeMultimap map = TreeMultimap.create();
        TreeMultimap map = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        for (String word : arr) {
            map.put(word.length(), word);
        }

        Iterator<Integer> iter = map.keySet().iterator();
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            int key = iter.next();
            Set set = map.get(key);
            Iterator<String> iter2 = set.iterator();
            String s = "";
            while(iter2.hasNext()) {
                s += iter2.next() + ", ";
            }
            if(s.length() >= 2) {
                s = s.substring(0, s.length()-2);
            }
            col.append("key: " + key + " | value: " + s + "<br>");
        }
        return col.toString();
    }
    
    //Коллекция в коллекции
    private String collectionsLinkedHashMapsPreview2(String text) {
        String arr[] = cleanText(text).split(" ");
        Map<Integer, LinkedHashSet<String>> map = new HashMap(); //создаем map<key : value(collection)>
        int a = 0;
        LinkedHashSet<String> linkBox; //объявляем новую коллекцию которая внутри map => value(collection)
        for (String word : arr) {
            if (!map.containsKey(word.length())) { //если нет ключа с заданой длиной
                linkBox = new LinkedHashSet<>(); //создаем новую коллекцию пока пустую
            } else {
                linkBox = map.get(word.length());
            }
            linkBox.add(word); //добавляем в конец колекции word в value(collection)
            map.put(word.length(), linkBox); // добавляем в map <key : value(collection)>
        }

        TreeSet<Integer> ts = new TreeSet(Collections.reverseOrder()); //создаем TreeSet
        ts.addAll(map.keySet()); //помещаем туда ссылки на ключи из map TreeSet сортирует их по своим свойствам
//        Iterator<Integer> iter = map.keySet().iterator();
        Iterator<Integer> iter = ts.iterator(); //создаем итератор TreeSet-а
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            int key = iter.next();
            LinkedHashSet set = map.get(key); //создаем коллекцию set и записываем туда коллекцию value(collection) по ключю
            Iterator<String> iter2 = set.iterator(); //создаем второй итератор для коллекции set
            String s = "";
            while(iter2.hasNext()) {
                s += iter2.next() + ", ";
            }
            if(s.length() >= 2) {
                s = s.substring(0, s.length()-2);
                a=1;
            }
            col.append("key: " + key + " | value: " + s + "<br>");
            a=1;
        }
        return col.toString();
    }
    
    //Коллекция в коллекции
    private String collectionsLinkedHashMapsPreview1(String text) {
        String arr[] = cleanText(text).split(" ");
        Map<Integer, LinkedHashSet<String>> map = new TreeMap(Collections.reverseOrder());
        LinkedHashSet<String> linkBox; //объявляем новую коллекцию которая внутри map
        for (String word : arr) {
            if (!map.containsKey(word.length())) { //если нет ключа с заданой длиной
                linkBox = new LinkedHashSet<>(); //создаем новую коллекцию
            } else {
                linkBox = map.get(word.length());
            }
            linkBox.add(word);
            map.put(word.length(), linkBox);
        }

        Iterator<Entry<Integer, LinkedHashSet<String>>> iter = map.entrySet().iterator();  
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            Entry<Integer, LinkedHashSet<String>> e = iter.next();
            Iterator<String> iter2 = e.getValue().iterator();
            String s = "";
            while(iter2.hasNext()) {
                s += iter2.next() + ", ";
            }
            if(s.length() >= 2) {
                s = s.substring(0, s.length()-2);
            }
            col.append("key: " +e.getKey() + " | value: " + s + "<br>");
        }
        return col.toString();
    }
    
    private String collectionsLinkedHashMapsPreview(String text) {
        String arr[] = cleanText(text).split(" ");
        Map<Integer, String> map = new LinkedHashMap();
        for (String word : arr) {
            map.put(word.length(), word);
        }

        Iterator<Entry<Integer, String>> iter = map.entrySet().iterator();
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            Entry e = iter.next();
            col.append("key: " + e.getKey() + " | value: " + e.getValue() + "<br>");
        }
        return col.toString();
    }
    
    private String collectionsTreeMapsPreview(String text) {
        String arr[] = cleanText(text).split(" ");
        Map<Integer, String> map = new TreeMap(Collections.reverseOrder());
        for (String word : arr) {
            map.put(word.length(), word);
        }

        Iterator<Entry<Integer, String>> iter = map.entrySet().iterator();
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            Entry e = iter.next();
            col.append("key: " + e.getKey() + " | value: " + e.getValue() + "<br>");
        }
        return col.toString();
    }

    //Метод показывает работу коллекций map
    private String collectionsMapsPreview2(String text) {
        String arr[] = cleanText(text).split(" ");
        //Объявляем коллекцию map с типами Key : Value
        HashMap<String, Integer> map = new HashMap();
        for (String word : arr) {
            map.put(word, word.length());
        }
//        //Меняются местами типы
//        HashMap<Integer, String> map2 = new HashMap();
//        for (String word : arr) {
//            map2.put(word.length(), word);
//        }
        String col = "";
        Iterator<String> iter2 = map.keySet().iterator();
        while (iter2.hasNext()) {
            String key = iter2.next();
            col += "key: " + key + " | value: " + map.get(key) + "<br>";
        }
        return col; //map2.toString(); // map2.get(5); //выводим key : value
    }

    //Метод показывает работу коллекций map
    private String collectionsMapsPreview1(String text) {
        String arr[] = cleanText(text).split(" ");
        //Объявляем коллекцию map с типами Key : Value
        HashMap<String, Integer> map = new HashMap();
        for (String word : arr) {
            map.put(word, word.length());
        }
        String col = "";
        //Итератор для Entry - это запись где содержится Key : Value
        Iterator<Entry<String, Integer>> iter = map.entrySet().iterator(); //map.values().iterator(); //map.keySet().iterator();       
        while (iter.hasNext()) {
            Entry e = iter.next();
            col += "key: " + e.getKey() + " | value: " + e.getValue() + "<br>";
        }
        return col; //выводит key : value
    }

    //Метод показывает работу коллекций map
    private String collectionsMapsPreview(String text) {
        String arr[] = cleanText(text).split(" ");
        //Объявляем коллекцию map с типами Key : Value
        HashMap<String, Integer> map = new HashMap();
        for (String word : arr) {
            map.put(word, word.length());
        }
//        //Меняются местами типы
//        HashMap<Integer, String> map2 = new HashMap();
//        for (String word : arr) {
//            map2.put(word.length(), word);
//        }
        String col = "";
        Iterator<String> iter2 = map.keySet().iterator();
//        while(iter2.hasNext()) {
//            String key = iter2.next();
//            col += "key: " + key+ " | value: " + map.get(key) + "<br>";
//        }
        while (iter2.hasNext()) {
            col += "value: " + map.get(iter2.next()) + "<br>";
        }

        return col; //map2.toString(); // map2.get(5); //выводит value
    }

    //Метод показывает работу массива и колекций set
    private String collectionsPreview(String text) {
        String arr[] = cleanText(text).split(" ");
        String col1 = "", col2 = "", col3 = "", col4 = "", col5 = "";
        StringBuilder sb = new StringBuilder("<table border=\"1\";><tr>");
        ArrayList<String> arrlist = new ArrayList<>();
        HashSet<String> hashset = new HashSet<>();
        LinkedHashSet<String> linkedset = new LinkedHashSet<>();
        TreeSet<String> treeset = new TreeSet<>();
        TreeSet<String> treesetrevers = new TreeSet<>(Collections.reverseOrder());
        //распаковываем массив
        for (String word : arr) {
            arrlist.add(word); //выводит в порядке текста
            hashset.add(word); //выводит в непонятном порядке
            linkedset.add(word); //выводит в порядке текста
            treeset.add(word); //выводит по алфавиту
            treesetrevers.add(word); //выводит по алфавиту в обратном порядке
        }
//        for (String word : arrlist){
//            col1 += word + "<br>";
//        }

        //наполняем столбцы
        for (int i = 0; i < arrlist.size(); i++) {
            col1 += arrlist.get(i) + "<br>";
        }
        Iterator<String> iter = hashset.iterator(); // можно использовать ListIterator
        while (iter.hasNext()) {
            col2 += iter.next() + "<br>";
        }
        iter = linkedset.iterator();
        while (iter.hasNext()) {
            col3 += iter.next() + "<br>";
        }
        iter = treeset.iterator();
        while (iter.hasNext()) {
            col4 += iter.next() + "<br>";
        }
        iter = treesetrevers.iterator();
        while (iter.hasNext()) {
            col5 += iter.next() + "<br>";
        }

        sb.append("<td>").append(col1).append("</td><td>").append(col2)
                .append("</td><td>").append(col3).append("</td><td>").append(col4)
                .append("</td><td>").append(col5).append("</td>");
        sb.append("</tr></table>");
        return sb.toString();
    }

    //Метод получает текст и разбирает его на колонки
    private String sortToSeveralWordsColumn1(String text) {
        String arr[] = cleanText(text).split(" ");
        String col1 = "", col2 = "", col3 = "", col4 = "";
        StringBuilder sb = new StringBuilder("<table border=\"1\";><tr>");
        for (String word : arr) {
            //слова с заглавной буквой в регистре 0
            if (Character.isUpperCase(word.charAt(0))) {
                col1 += word + "<br>";
            }
            //слова начинающиеся на цифру
            if (Character.isDigit(word.charAt(0))) {
                col2 += word + "<br>";
            }
            //слова длиной больше 1 символа и содержащие одну цифру в регистре 1
            if (word.length() > 1 && Character.isDigit(word.charAt(1))) {
                col3 += word + "<br>";
            }
            //слова со всеми заглавнми буквами
            if (Character.isUpperCase(word.charAt(word.length() - 1))) {
                col4 += word + "<br>";
            }
        }
        sb.append("<td>").append(col1).append("</td><td>").append(col2)
                .append("</td><td>").append(col3).append("</td><td>").append(col4)
                .append("</td>");
        sb.append("</tr></table>");
        return sb.toString();
    }

    //Метод получает текст и разбирает его на колонки
    private String sortToSeveralWordsColumn(String text) {
        String arr[] = cleanText(text).split(" ");
        String col1 = "", col2 = "", col3 = "", col4 = "";
        StringBuilder sb = new StringBuilder("<table border=\"1\";><tr>");
        for (String word : arr) {
            //слова с длиной 3-4 символа
            if (word.length() > 2 && word.length() < 5) {
                col1 += word + "<br>";
            }
            //слова начинающиеся на букву А и П
            if (word.startsWith("а") || word.startsWith("п")) {
                col2 += word + "<br>";
            }
            //слова заканчивающиеся на букву О
            if (word.endsWith("о")) {
                col3 += word + "<br>";
            }
            //слова содержащие в себе "сыг"
            if (word.contains("сыг")) {
                col4 += word + "<br>";
            }
        }
        sb.append("<td>").append(col1).append("</td><td>").append(col2)
                .append("</td><td>").append(col3).append("</td><td>").append(col4)
                .append("</td>");
        sb.append("</tr></table>");
        return sb.toString();
    }

    //Метод получает текст и выводим слова в столбик
    private String arrayToOneWordsColumn(String text) {
        String textarr[] = cleanText(text).split(" ");
        //Вариант с циклом, который заполняет строку из значений массива
//        String res = "";
//        for (int i=0; i < arr.length; i++) {
//            res += arr[i]+"<br>";
//        }
        //Использование StringBuilder для создания строки, предпочтительней!
        StringBuilder sbRes = new StringBuilder();
        for (String arr : textarr) {
            sbRes.append(arr).append("<br>");
        }
        return sbRes.toString();
    }

    //Метод обрабатывает текст путем замены одних символов на другие
    private String cleanText(String text) {
        return text.replaceAll(",", "").replaceAll("[.]", "").replaceAll(" - ", " ")
                .replaceAll("\r\n", " ").replaceAll("\\(", "").replaceAll("[)]", "")
                .replaceAll("\t", " ").replaceAll("( )+", " ");
    }

    //Метод суммирует значения внутри массива но выводит текс в браузер через PrintWriter
    private void sumWeb(PrintWriter out) throws IOException {
        double nums[] = {1, 2, 3.5};
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }

        out.println(sum);
    }

    //Метод суммирует значения внутри массива но выводит текс в браузер через PrintWriter 
    private void sumarrWeb(HttpServletResponse response) throws IOException {
        double nums[] = {1, 2, 3.5};
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(sum);
        }
    }

    //Метод суммирует значения внутри массива
    private double sumarr() {
        double nums[] = {1, 2, 3.5};
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        return sum;
    }

    private boolean palendroid(String text) {
        StringBuilder sb = new StringBuilder(text);
        String rev = sb.reverse().toString();
        if (text.equals(rev)) {
            return true;
        }
        return false;
    }

    //Метод увеличивает и уменьшает значение на 1
    private void incAndDec(int a) {
        int b = 0;
        while (b < a) {
            b++;
            System.out.println(b);
        }

        System.out.println("****");
        do {
            System.out.println(b);
            b--;
        } while (b >= 1);
    }

    //Метод вычисляет 2 числа
    private void calculator(int a, int b, String action) {
        if (action.equals("+")) {
            System.out.println(a + b);
        } else if (action.equals("-")) {
            System.out.println(a - b);
        } else if (action.equals("*")) {
            System.out.println(a * b);
        } else if (action.equals("/")) {
            System.out.println(a / b);
        }
    }

    //Метод выводит в консоль четные цифры
    private void doJob1(int a) {
        for (int i = 0; i < a; i++) {
            if (i % 2 != 0) {
                continue;
            }
            System.out.println(i);
        }
    }

    //Метод создает сессию и включает счетчик
    private Integer sessionCounter(HttpServletRequest request) throws IOException {
        int count = 0;
        HttpSession session = request.getSession();

        if (session.getAttribute("count") != null) {
            count = (int) session.getAttribute("count");
        }

        count++;
        session.setAttribute("count", count);
        return count;
    }

//    
//
//
//
//    
//    
//    
//    private String collection3(String text) {
//        String arr[] = cleanText(text).split(" ");
////        Map<Integer, String> map = new LinkedHashMap();
//        Map<Integer, String> map = new TreeMap(Collections.reverseOrder());
//        for (String word : arr) {
//            map.put(word.length(), word);
//        }
//
//        Iterator<Entry<Integer, String>> iter = map.entrySet().iterator();
//        StringBuilder col = new StringBuilder();
//        while (iter.hasNext()) {
//            Entry e = iter.next();
//            col.append("key: " + e.getKey() + " | value: " + e.getValue() + "<br>");
//        }
//        return col.toString();
//    }
//    private String collection4(String text) {
//        String arr[] = cleanText(text).split(" ");
//        Map<Integer, LinkedHashSet<String>> map = new TreeMap(Collections.reverseOrder());
//        LinkedHashSet<String> linkBox;
//        for (String word : arr) {
//            if (!map.containsKey(word.length())) {
//                linkBox = new LinkedHashSet<>(); // 
//
//            } else {
//                linkBox = map.get(word.length());
//            }
//            linkBox.add(word);
//            map.put(word.length(), linkBox);
//        }
//
//        Iterator<Entry<Integer, LinkedHashSet<String>>> iter = map.entrySet().iterator();  
//        StringBuilder col = new StringBuilder();
//        while (iter.hasNext()) {
//            Entry<Integer, LinkedHashSet<String>> e = iter.next();
//            Iterator<String> iter2 = e.getValue().iterator();
//            String s = "";
//            while(iter2.hasNext()) {
//                s += iter2.next() + ", ";
//            }
//            col.append("key: " +e.getKey() + " | value: " + s + "<br>");
//        }
//        return col.toString();
//    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

////        
        //
//        response.setContentType("text/html;charset=UTF-8");
//        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
