package test_folder;

import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/StringServlet")
public class StingServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String query = request.getParameter("query");
        String text = request.getParameter("text");

        String output = searchWord5(query, text);

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");
            out.println("<link href=\"WEB-INF/css/bootstrap.grid.css\" rel=\"stylesheet\" type=\"text/css\"/>");
            out.println("<link href=\"WEB-INF/css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\"/>");
            out.println("<link href=\"WEB-INF/css/styles.css\" rel=\"stylesheet\" type=\"text/css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println(output);
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    private String highlite(String sentenceToLower, String sentenceToUp, String query[]) {
        String before = "<b><font color=\"#2554C7\">", after = "</font></b>";
        String result = sentenceToUp;
        int queryStart = 0;
        int queryEnd;
        int shift = 0;

        TreeMap<Integer, Integer> map = new TreeMap();
        for (String query1 : query) {
            queryStart = 0;
            while (queryStart > -1) {
                queryStart = sentenceToLower.indexOf(query1, queryStart);
                if (queryStart > -1) {
                    map.put(queryStart, queryStart + query1.length());
                    queryStart++;
                }
            }
        }

        Iterator<Entry<Integer, Integer>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Entry e = iter.next();
            queryStart = (int) e.getKey();
            queryEnd = (int) e.getValue();
            result = result.substring(0, queryStart + shift) + before 
                    + result.substring(queryStart + shift, queryEnd + shift) 
                    + after + result.substring(queryEnd + shift);
            shift = 36 + shift;

        }

        return result.toString();
    }

    private String searchWord5(String query, String text) {
        String result = "not found";
        String textToUp = text;
        text = text.toLowerCase();
        query = query.toLowerCase();
        String splitQuery[] = query.split(" ");
        int start = 0;
        int end = 0;
        String sentenceToUp = "";
        String sentenceToLower = "";

        TreeMultimap<String, String> map = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        TreeMultimap<Integer, String> map2 = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        for (int i = 0; i < splitQuery.length; i++) { //Переберает каждое слово из запроса
            int queryPos = 0;

            while (queryPos > -1) {
                queryPos = text.indexOf(splitQuery[i], queryPos); //получаем позицию запроса 
                if (queryPos > -1) { //если запрос найден
                    start = text.lastIndexOf(". ", queryPos); //получаем начальную точку предложения
                    if (start == -1) { //если это первое слово, устанавливаем начало с 0
                        start = 0;
                    } else {
                        start += 2;
                    }
                    end = text.indexOf(". ", queryPos); //получаем конечную точку предложения
                    if (end == -1) { //если в конце текста нет точки выставляем конец предложения
                        end = text.length();
                    } else {
                        end += 2;
                    }
                    queryPos++;
                    sentenceToLower = text.substring(start, end);
                    sentenceToUp = textToUp.substring(start, end); //выделаем предложение через начало и конец
                    map.put(splitQuery[i], highlite(sentenceToLower, sentenceToUp, splitQuery)); //сохраняем предложение
                }
            }
        }

        Iterator<String> iter = map.keySet().iterator();
        while (iter.hasNext()) {
            String word = iter.next();
            map2.put(map.get(word).size(), word);
        }

        Iterator<String> iter2 = map2.values().iterator();
        StringBuilder col = new StringBuilder();
        while (iter2.hasNext()) { //итератор - колличество предложений
            String key = iter2.next();
            Set set = map.get(key);
            Iterator<String> iter3 = set.iterator();
            String s = " ";
            while (iter3.hasNext()) {
                s += "<tr><td><b>value: </b>" + iter3.next() + "</td></tr>";
                int i = 0;
            }
            col.append("<table class=\"table\"><tr><td><b>key: </b>" + key + "</td></tr>" + s + "</table>");
            int i = 0;
        }
        return col.toString();
    }

    private String searchWord4(String query, String text) {
        String result = "not found";
        String textToUp = text;
        text = text.toLowerCase();
        query = query.toLowerCase();
        String splitQuery[] = query.split(" ");
        int start = 0;
        int end = 0;
        String sentence = "";

        TreeMultimap<String, String> map = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        for (int i = 0; i < splitQuery.length; i++) { //Переберает каждое слово из запроса
            int queryPos = 0;

            while (queryPos > -1) {
                queryPos = text.indexOf(splitQuery[i], queryPos); //получаем позицию запроса 
                if (queryPos > -1) { //если запрос найден
                    start = text.lastIndexOf(". ", queryPos); //получаем начальную точку предложения
                    if (start == -1) { //если это первое слово, устанавливаем начало с 0
                        start = 0;
                    } else {
                        start += 2;
                    }
                    end = text.indexOf(". ", queryPos); //получаем конечную точку предложения
                    if (end == -1) { //если в конце текста нет точки выставляем конец предложения
                        end = text.length();
                    } else {
                        end += 2;
                    }
                    queryPos++;
                    sentence = textToUp.substring(start, end); //выделаем предложение через начало и конец
                    map.put(splitQuery[i], sentence); //сохраняем предложение
                } else {
                    break;
                }
            }
        }
        Iterator<String> iter = map.keySet().iterator();
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            String key = iter.next();
            Set set = map.get(key);
            Iterator<String> iter2 = set.iterator();
            String s = " ";
            while (iter2.hasNext()) {
                s += "<tr><td><b>value: </b>" + iter2.next() + "</td></tr>";
                int i = 0;
            }
            col.append("<table class=\"table\"><tr><td><b>key: </b>" + key + "</td></tr>" + s + "</table>");
            int i = 0;
        }
        return col.toString();
    }

    private String searchWord3(String query, String text) {
        String result = "not found";
        text = text.toLowerCase();
        query = query.toLowerCase();
        String splitQuery[] = query.split(" ");
        int count = 0;
        TreeMultimap map = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
        for (int i = 0; i < splitQuery.length; i++) {
            count = searchCounter(splitQuery[i], text);
            if (count > 0) {
                map.put(count, splitQuery[i]);
            }
        }
        Iterator<Integer> iter = map.keySet().iterator();
        StringBuilder col = new StringBuilder();
        while (iter.hasNext()) {
            int key = iter.next();
            Set set = map.get(key);
            Iterator<String> iter2 = set.iterator();
            String s = "";
            while (iter2.hasNext()) {
                s += iter2.next() + ", ";
            }
            if (s.length() >= 2) {
                s = s.substring(0, s.length() - 2);
            }
            col.append("key: " + key + " | value: " + s + "<br>");
        }
        return col.toString();
    }

    private String searchWord2(String query, String text) {
        String result = "not found";
        text = text.toLowerCase();
        query = query.toLowerCase();
        String splitQuery[] = query.split(" ");
        int count = 0;
        for (int i = 0; i < splitQuery.length; i++) {
            int x = 0, from = 0;
            while (x > -1) {
                x = text.indexOf(splitQuery[i], from);
                from = x+1;
                if (x > -1) {
                    count++;
                }
            }
        }

        return "Found: " + count;
    }

    private String searchWord1(String query, String text) {
        String result = "not found";
        text = text.toLowerCase();
        query = query.toLowerCase();

        int x = 0, from = 0;
        int i = 0;
        while (x > -1) {
            x = text.indexOf(query, from);
            from = x+1;
            if (x > -1) {
                i++;
            }
        }

        return "Found: " + i;
    }

    private String searchWord(String query, String text) {
        String result = "not found";
        text = text.toLowerCase();
        query = query.toLowerCase();
//        if(text.contains(query)){
//            return "found";
//        }

        int x = text.indexOf(query);
        if (x > -1) {
            return "found at " + x;
        }

        return result;
    }

    private int searchCounter(String query, String text) {
        int x = 0;
        int count = 0, from =0;
        while (x > -1) {
            x = text.indexOf(query, from);
            from = x+1;
            if (x > -1) {
                count++;
            }
        }
        return count;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
