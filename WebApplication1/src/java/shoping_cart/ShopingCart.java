package shoping_cart;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/shop") //указываем путь в адресной строке через который ссылаемся на этот сервлет
public class ShopingCart extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        //Получаем данные из формы shopcard.jsp
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String action = request.getParameter("action");
        int code;
        int price;
        //если значение не числовое выкидываем ошибку
        try {
            code = Integer.parseInt(request.getParameter("code")); //сделать на js проверку или установку значения по умолчанию 0
        } catch (NumberFormatException ex) {
            code = 0;
            ex.printStackTrace(System.err);
            System.out.println("Error");
        }

        try {
            price = Integer.parseInt(request.getParameter("price"));
        } catch (NumberFormatException ex) {
            price = 0;
            ex.printStackTrace(System.err);
            System.out.println("Error");
        }

//        switch(action) {
//            case "add":
//                
//            
//        }
        ArrayList<Product> productList; //создаем ArrayList с типом Product
//        HttpSession session = request.getSession(); // Инициализируем сессию
        ServletContext application = getServletContext();
        Product product;

        if (application.getAttribute("productList") != null) {
            productList = (ArrayList<Product>) application.getAttribute("productList");
        } else {
            productList = new ArrayList<>();
        }

        action = action.toLowerCase();
        switch (action) {
            case "add":
                product = new Product(name, description, price); //Используем конструктор через который сразу добавляем поля
                code = productList.size() + 1; //создаем следующий номер продукта последного в списке
                product.setCode(code);
                productList.add(product);
                break;
            case "remove":
                code--;
                if (!productList.isEmpty() && code < productList.size()) {
                    productList.remove(code);
                }
                int i = 0;
                break;
            case "edit":
                code--;
                if (!productList.isEmpty() && code <= productList.size()) {
                    product = productList.get(code);
                    product.setName(name); //Сделать проверки
                    product.setDescription(description);
                    product.setPrice(price);
                }
                break;
        }

        application.setAttribute("productList", productList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shopcard.jsp");
        dispatcher.forward(request, response);
      
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ShopingCart</title>");
//            out.println("</head>");
//            out.println("<body>");
//            Iterator<Product> iter = productList.iterator();
//            while (iter.hasNext()) {
//                product = iter.next(); //Product p = (Product) iter.next(); приведение типа
//                out.println(product.getName() + " " + product.getDescription());
//            }
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
